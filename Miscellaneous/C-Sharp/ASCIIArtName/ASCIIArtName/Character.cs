﻿namespace ASCIIArtName
{
    public static class Character
    {

public static string asciiCharA1=(@"  .--.   ");
public static string asciiCharA2=(@" / {} \  ");
public static string asciiCharA3=(@"/  /\  \ ");
public static string asciiCharA4=(@"`-'  `-' ");                
         
        

public static string asciiCharB1=(@".----.  ");
public static string asciiCharB2=(@"| {}  } ");
public static string asciiCharB3=(@"| {}  } ");
public static string asciiCharB4=(@"`----'  ");




public static string asciiCharC1 = (@" .---. ");
public static string asciiCharC2 = (@"/  ___}");
public static string asciiCharC3 = (@"\     }");
public static string asciiCharC4 = (@" `---' ");


public static string asciiCharD1 = (@".----. ");
public static string asciiCharD2 = (@"| {}  \");
public static string asciiCharD3 = (@"|     /");
public static string asciiCharD4 = (@"`----' ");

public static string asciiCharE1 = (@".----.");
public static string asciiCharE2 = (@"| {_  ");
public static string asciiCharE3 = (@"| {__ ");
public static string asciiCharE4 = (@"`----'");

public static string asciiCharF1 = (@".----.");
public static string asciiCharF2 = (@"| {_  ");
public static string asciiCharF3 = (@"| |   ");
public static string asciiCharF4 = (@"`-'   ");

public static string asciiCharG1 = (@" .---.  ");
public static string asciiCharG2 = (@"/   __}");
public static string asciiCharG3 = (@"\  {_ }");
public static string asciiCharG4 = (@" `---'  ");


public static string asciiCharH1 = (@".-. .-.");
public static string asciiCharH2 = (@"| {_} |");
public static string asciiCharH3 = (@"| { } |");
public static string asciiCharH4 = (@"`-' `-'");

public static string asciiCharI1 = (@".-.");
public static string asciiCharI2 = (@"| |");
public static string asciiCharI3 = (@"| |");
public static string asciiCharI4 = (@"`-'");

public static string asciiCharJ1 = (@"   .-.");
public static string asciiCharJ2 = (@".-.| |");
public static string asciiCharJ3 = (@"| {} |");
public static string asciiCharJ4 = (@"`----'");

public static string asciiCharK1 = (@".-. .-.");
public static string asciiCharK2 = (@"| |/ / ");
public static string asciiCharK3 = (@"| |\ \ ");
public static string asciiCharK4 = (@"`-' `-'");

public static string asciiCharL1 = (@".-.   ");
public static string asciiCharL2 = (@"| |   ");
public static string asciiCharL3 = (@"| `--.");
public static string asciiCharL4 = (@"`----'");

public static string asciiCharM1 = (@".-.   .-.");
public static string asciiCharM2 = (@"|  `.'  |");
public static string asciiCharM3 = (@"| |\ /| |");
public static string asciiCharM4 = (@"`-' ` `-'");


public static string asciiCharN1 = (@".-. .-.");
public static string asciiCharN2 = (@"|  `| |");
public static string asciiCharN3 = (@"| |\  |");
public static string asciiCharN4 = (@"`-' `-'");

public static string asciiCharO1 = (@" .----. ");
public static string asciiCharO2 = (@"/  {}  \");
public static string asciiCharO3 = (@"\      /");
public static string asciiCharO4 = (@" `----' ");

public static string asciiCharP1 = (@".----. ");
public static string asciiCharP2 = (@"| {}  }");
public static string asciiCharP3 = (@"| .--' ");
public static string asciiCharP4 = (@"`-'    ");

public static string asciiCharR1 = (@".----. ");
public static string asciiCharR2 = (@"| {}  }");
public static string asciiCharR3 = (@"| .-. \");
public static string asciiCharR4 = (@"`-' `-'");

public static string asciiCharS1 = (@" .----.");
public static string asciiCharS2 = (@"{ {__  ");
public static string asciiCharS3 = (@".-._} }");
public static string asciiCharS4 = (@"`----' ");

public static string asciiCharT1 = (@" .---. ");
public static string asciiCharT2 = (@"{_   _}");
public static string asciiCharT3 = (@"  | |  ");
public static string asciiCharT4 = (@"  `-'  ");

public static string asciiCharU1 = (@".-. .-.");
public static string asciiCharU2 = (@"| { } |");
public static string asciiCharU3 = (@"| {_} |");
public static string asciiCharU4 = (@"`-----'");

public static string asciiCharQ1 = (@" .----. ");
public static string asciiCharQ2 = (@"/  {}  \");
public static string asciiCharQ3 = (@"\      /");
public static string asciiCharQ4 = (@" `-----`");

public static string asciiCharV1 = (@".-. .-.");
public static string asciiCharV2 = (@"| | | |");
public static string asciiCharV3 = (@"\ \_/ /");
public static string asciiCharV4 = (@" `---' ");

public static string asciiCharW1 = (@".-. . .-.");
public static string asciiCharW2 = (@"| |/ \| |");
public static string asciiCharW3 = (@"|  .'.  |");
public static string asciiCharW4 = (@"`-'   `-'");

public static string asciiCharX1 = (@".-.  .-.");
public static string asciiCharX2 = (@" \ \/ / ");
public static string asciiCharX3 = (@" / /\ \ ");
public static string asciiCharX4 = (@"`-'  `-'");

public static string asciiCharY1 = (@".-.  .-.");
public static string asciiCharY2 = (@" \ \/ / ");
public static string asciiCharY3 = (@"  }  {  ");
public static string asciiCharY4 = (@"  `--'  ");

public static string asciiCharZ1 = (@" .---. ");
public static string asciiCharZ2 = (@"{_   / ");
public static string asciiCharZ3 = (@" /    }");
public static string asciiCharZ4 = (@" `---' ");



    }
}

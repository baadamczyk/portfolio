﻿using System;

namespace ASCIIArtName
{
    public static class Intro
    {
        
    public static void drawIntroImage()
        {           
            {
                Console.SetWindowSize(150, 30);

                Console.WriteLine("                                        ____________________________________________________________\n"); //60 znaków + 10 znaków pustych (spacja)
                Console.WriteLine("                                        |                    ASCII NAME GENERATOR                  |\n");
                Console.WriteLine("                                        ____________________________________________________________\n");
                Console.WriteLine("                                        Wprowadz swoje imie. Przekonwertuje je na zacny ASCIIart!\n");
                Console.WriteLine("                                            (CYFRY I ZNAKI SPECJALNE NIE BEDA UWZGLEDNIANE!)\n");
               
            }
        }
    }
}

﻿namespace ASCIIArtName
{
    public class Name
    {
        public string nameString;
        public char[] nameArray;
        public string line1= "";
        public string line2= "";
        public string line3= "";
        public string line4= "";

    public void createName(string nameString)
    {
        nameArray = nameString.ToCharArray();

        foreach(char character in nameArray)
        {
            switch(character)
            {
                case 'a':
                    line1+=Character.asciiCharA1;
                    line2+=Character.asciiCharA2;
                    line3+=Character.asciiCharA3;
                    line4+=Character.asciiCharA4;
                    break;
                case 'b':
                    line1+=Character.asciiCharB1;
                    line2+=Character.asciiCharB2;
                    line3+=Character.asciiCharB3;
                    line4+=Character.asciiCharB4;

                    break;
                case 'c':
                    line1+=Character.asciiCharC1;
                    line2+=Character.asciiCharC2;
                    line3+=Character.asciiCharC3;
                    line4+=Character.asciiCharC4;
                    break;
                case 'd':
                    line1+=Character.asciiCharD1;
                    line2+=Character.asciiCharD2;
                    line3+=Character.asciiCharD3;
                    line4+=Character.asciiCharD4;
                    break;
                case 'e':
                    line1+=Character.asciiCharE1;
                    line2+=Character.asciiCharE2;
                    line3+=Character.asciiCharE3;
                    line4+=Character.asciiCharE4;
                    break;
                case 'f':
                    line1+=Character.asciiCharF1;
                    line2+=Character.asciiCharF2;
                    line3+=Character.asciiCharF3;
                    line4+=Character.asciiCharF4;
                    break;
                case 'g':
                    line1+=Character.asciiCharG1;
                    line2+=Character.asciiCharG2;
                    line3+=Character.asciiCharG3;
                    line4+=Character.asciiCharG4;
                    break;
                case 'h':
                    line1+=Character.asciiCharH1;
                    line2+=Character.asciiCharH2;
                    line3+=Character.asciiCharH3;
                    line4+=Character.asciiCharH4;
                    break;
                case 'i':
                    line1+=Character.asciiCharI1;
                    line2+=Character.asciiCharI2;
                    line3+=Character.asciiCharI3;
                    line4+=Character.asciiCharI4;
                    break;
                case 'j':
                    line1+=Character.asciiCharJ1;
                    line2+=Character.asciiCharJ2;
                    line3+=Character.asciiCharJ3;
                    line4+=Character.asciiCharJ4;
                    break;
                case 'k':
                    line1+=Character.asciiCharK1;
                    line2+=Character.asciiCharK2;
                    line3+=Character.asciiCharK3;
                    line4+=Character.asciiCharK4;
                    break;
                case 'l':
                    line1+=Character.asciiCharL1;
                    line2+=Character.asciiCharL2;
                    line3+=Character.asciiCharL3;
                    line4+=Character.asciiCharL4;
                    break;
                case 'm':
                    line1+=Character.asciiCharM1;
                    line2+=Character.asciiCharM2;
                    line3+=Character.asciiCharM3;
                    line4+=Character.asciiCharM4;
                    break;
                case 'n':
                    line1+=Character.asciiCharN1;
                    line2+=Character.asciiCharN2;
                    line3+=Character.asciiCharN3;
                    line4+=Character.asciiCharN4;
                    break;
                case 'o':
                    line1+=Character.asciiCharO1;
                    line2+=Character.asciiCharO2;
                    line3+=Character.asciiCharO3;
                    line4+=Character.asciiCharO4;
                    break;
                case 'p':
                    line1+=Character.asciiCharP1;
                    line2+=Character.asciiCharP2;
                    line3+=Character.asciiCharP3;
                    line4+=Character.asciiCharP4;
                    break;
                case 'r':
                    line1+=Character.asciiCharR1;
                    line2+=Character.asciiCharR2;
                    line3+=Character.asciiCharR3;
                    line4+=Character.asciiCharR4;
                    break;
                case 's':
                    line1+=Character.asciiCharS1;
                    line2+=Character.asciiCharS2;
                    line3+=Character.asciiCharS3;
                    line4+=Character.asciiCharS4;
                    break;
                case 't':
                    line1+=Character.asciiCharT1;
                    line2+=Character.asciiCharT2;
                    line3+=Character.asciiCharT3;
                    line4+=Character.asciiCharT4;
                    break;
                case 'u':
                    line1+=Character.asciiCharU1;
                    line2+=Character.asciiCharU2;
                    line3+=Character.asciiCharU3;
                    line4+=Character.asciiCharU4;
                    break;
                case 'v':
                    line1+=Character.asciiCharV1;
                    line2+=Character.asciiCharV2;
                    line3+=Character.asciiCharV3;
                    line4+=Character.asciiCharV4;
                    break;
                case 'w':
                    line1+=Character.asciiCharW1;
                    line2+=Character.asciiCharW2;
                    line3+=Character.asciiCharW3;
                    line4+=Character.asciiCharW4;
                    break;
                case 'x':
                    line1+=Character.asciiCharX1;
                    line2+=Character.asciiCharX2;
                    line3+=Character.asciiCharX3;
                    line4+=Character.asciiCharX4;
                    break;
                case 'y':
                    line1+=Character.asciiCharY1;
                    line2+=Character.asciiCharY2;
                    line3+=Character.asciiCharY3;
                    line4+=Character.asciiCharY4;
                    break;
                case 'z':
                    line1+=Character.asciiCharZ1;
                    line2+=Character.asciiCharZ2;
                    line3+=Character.asciiCharZ3;
                    line4+=Character.asciiCharZ4;
                    break;
                default:
                    break;
            }
        }
    }
    }

    
}

﻿using System;

namespace ASCIIArtName
{
    class Program
    {
        static void Main(string[] args)
        {
            Intro.drawIntroImage();  
                      
            Name userName = new Name();
            start:

            userName.nameString = Console.ReadLine().ToLower();
            if(userName.nameString.Length>19)
            {
                Console.WriteLine("                         Maksymalna dlugosc imienia to 19 znaków! Wprowadz krotsze imie! [NACISNIJ DOWOLNY KLAWISZ]");
                Console.ReadKey();

                userName.nameString = "";
                Console.Clear();
                Intro.drawIntroImage();
                goto start;
            }

            userName.createName(userName.nameString);
            
            Console.WriteLine("\n"+userName.line1);
            Console.WriteLine(userName.line2);
            Console.WriteLine(userName.line3);
            Console.WriteLine(userName.line4);
                       
            Console.ReadKey();
        }
    }
}

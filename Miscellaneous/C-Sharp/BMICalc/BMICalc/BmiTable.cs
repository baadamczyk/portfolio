﻿namespace BMICalc
{
    public class BmiResult
    {
        public double bmiResult;
        
        public int bmiCheck(double bmi)
        {
            if (bmi < 18.5) return 1;
            else if (bmi < 24.9) return 2;
            else if (bmi < 29.9) return 3;
            else if (bmi < 34.9) return 4;
            else if (bmi < 39.9) return 5;
            else return 6;                    
        }        
    }
}

﻿namespace BMICalc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.heightInput = new System.Windows.Forms.MaskedTextBox();
            this.weightInput = new System.Windows.Forms.MaskedTextBox();
            this.heightInputLabel = new System.Windows.Forms.Label();
            this.weightInputLabel = new System.Windows.Forms.Label();
            this.calculateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // heightInput
            // 
            this.heightInput.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.heightInput.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.heightInput.Location = new System.Drawing.Point(34, 34);
            this.heightInput.Mask = "000";
            this.heightInput.Name = "heightInput";
            this.heightInput.PromptChar = ' ';
            this.heightInput.Size = new System.Drawing.Size(100, 20);
            this.heightInput.TabIndex = 0;
            this.heightInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // weightInput
            // 
            this.weightInput.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.weightInput.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.weightInput.Location = new System.Drawing.Point(34, 82);
            this.weightInput.Mask = "000";
            this.weightInput.Name = "weightInput";
            this.weightInput.PromptChar = ' ';
            this.weightInput.Size = new System.Drawing.Size(100, 20);
            this.weightInput.TabIndex = 1;
            this.weightInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // heightInputLabel
            // 
            this.heightInputLabel.AutoSize = true;
            this.heightInputLabel.Location = new System.Drawing.Point(156, 37);
            this.heightInputLabel.Name = "heightInputLabel";
            this.heightInputLabel.Size = new System.Drawing.Size(63, 13);
            this.heightInputLabel.TabIndex = 2;
            this.heightInputLabel.Text = "Wzrost [cm]";
            // 
            // weightInputLabel
            // 
            this.weightInputLabel.AutoSize = true;
            this.weightInputLabel.Location = new System.Drawing.Point(156, 82);
            this.weightInputLabel.Name = "weightInputLabel";
            this.weightInputLabel.Size = new System.Drawing.Size(57, 13);
            this.weightInputLabel.TabIndex = 3;
            this.weightInputLabel.Text = "Waga [kg]";
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(82, 138);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 4;
            this.calculateButton.Text = "Policz";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(228, 197);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.weightInputLabel);
            this.Controls.Add(this.heightInputLabel);
            this.Controls.Add(this.weightInput);
            this.Controls.Add(this.heightInput);
            this.Name = "Form1";
            this.Text = "Kalkulator BMI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox heightInput;
        private System.Windows.Forms.MaskedTextBox weightInput;
        private System.Windows.Forms.Label heightInputLabel;
        private System.Windows.Forms.Label weightInputLabel;
        private System.Windows.Forms.Button calculateButton;
    }
}


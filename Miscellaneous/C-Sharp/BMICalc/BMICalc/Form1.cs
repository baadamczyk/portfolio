﻿using System;
using System.Windows.Forms;

namespace BMICalc
{
    public partial class Form1 : Form
    {
        public double weight { get; set; }
        public double height { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        public void clearForm()
        {
            heightInput.Text = null;
            weightInput.Text = null;
            heightInput.Focus();
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {            
            double bmi;

            GetDataFromTextboxes();

            bmi = CalculateBMI(weight, height);
            ShowResult(bmi);
        }

        public void ShowResult(double bmi)
        {
            BmiResult userResult = new BmiResult();
            userResult.bmiResult = bmi;

            switch (userResult.bmiCheck(bmi))
            {
                case 1:
                    MessageBox.Show("Twój indeks BMI wynosi: " + bmi + "\nMasz niedowagę!");
                    clearForm();
                    break;
                case 2:
                    MessageBox.Show("Twój indeks BMI wynosi: " + bmi + "\nTwoja waga jest prawidłowa!");
                    clearForm();
                    break;
                case 3:
                    MessageBox.Show("Twój indeks BMI wynosi: " + bmi + "\nMasz nadwagę!");
                    clearForm();
                    break;
                case 4:
                    MessageBox.Show("Twój indeks BMI wynosi: " + bmi + "\nCierpisz na otyłość I-go stopnia!");
                    clearForm();
                    break;
                case 5:
                    MessageBox.Show("Twój indeks BMI wynosi: " + bmi + "\nCierpisz na otyłość II-go stopnia!");
                    clearForm();
                    break;
                case 6:
                    MessageBox.Show("Twój indeks BMI wynosi: " + bmi + "\nCierpisz na otyłość III-go stopnia!");
                    clearForm();
                    break;
                default:
                    clearForm();
                    break;
            }
        }

        private void GetDataFromTextboxes()
        {
            weight = Convert.ToDouble(weightInput.Text);
            height = Convert.ToDouble(heightInput.Text);
        }

        private double CalculateBMI(double weight, double height)
        {
            double bmi = ((weight / (height * height)) * 1e4);
            return Math.Round(bmi, 2);
        }
    }
}


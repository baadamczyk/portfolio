﻿using System;

namespace CurrentyConverter
{
    static class Conversion
    {      
        public static double ConvertCurrency(double currencyInputValue, int CurrencyA, int CurrencyB)
        {
            double PLNvalue;
            double conversionResult;

            if (IsCurrencyTheSame(CurrencyA, CurrencyB))
            {
                return currencyInputValue;
            }
            else
            {
                CalculatePLNValue(CurrencyA);
                PLNvalue = (currencyInputValue * Currency.currencyArray[CurrencyA]);

                if (CurrencyB == 0)
                {
                    return PLNvalue;
                }
                else
                {
                    conversionResult = (PLNvalue / Currency.currencyArray[CurrencyB]);

                    return conversionResult;
                }
            }
         }

        private static bool IsCurrencyTheSame(int CurrencyA, int CurrencyB)
        {
            if (Currency.currencyArray[CurrencyA] == Currency.currencyArray[CurrencyB]) return true;
            else return false;
        }
    }
}

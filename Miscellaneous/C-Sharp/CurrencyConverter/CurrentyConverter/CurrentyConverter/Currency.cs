﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurrentyConverter
{
    class Currency
    {
        public enum CurrencyList
        {
            PLN,
            USD,
            EUR,
            CHF,
            GBP,
            RUB,
            JPY,
            CNY,
            KRW,
            CZK,
            DKK,
            NOK
        }
        public static double[] currencyArray = new double[12];

        //Ustalenie przeliczników dla walut. Bazowa jednostka to PLN.                   

        public static void UpdateCurrency()
        {
            string[] dataFromFile;
            try
            {
            System.IO.File.ReadAllLines(@"db");
            }
            catch (System.IO.FileNotFoundException)
            {
                MessageBox.Show("Nie znaleziono pliku bazy!\nProgram zostanie zatrzymany!", "Currency Converter");
                Environment.Exit(-1);                
            }
           
            int parseError = 0;
            double parseResult = 0;

            //sprawdzanie, czy rekordy się zgadzają (liczba, format)
            dataFromFile = System.IO.File.ReadAllLines(@"db");
            if (dataFromFile.Length != 12)
            {
                MessageBox.Show("Plik bazy danych jest uszkodzony!\nSprawdź, czy wartości jest dokładnie 12 i czy wszystkie mają format zmiennoprzecinkowy!", "Currency Converter");
                Environment.Exit(-1);
            }
            else
            {
                    foreach (string line in dataFromFile)
                    {
                        double.TryParse(line, out parseResult); 
                    }
                    if (parseResult == 0) parseError++;
                }

                if (parseError != 0)
                {
                    MessageBox.Show("Plik bazy danych jest uszkodzony!\nSprawdź, czy wartości jest dokładnie 12 i czy wszystkie mają format zmiennoprzecinkowy!", "Currency Converter");
                    Environment.Exit(-1);
                }
                else
                {
                    for (int i = 0; i < 12; i++)
                    {
                        currencyArray[i] = double.Parse(dataFromFile[i]);
                    }
                }
            }


        }
    }


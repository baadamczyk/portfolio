﻿namespace CurrentyConverter
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sourceCurrencySelection = new System.Windows.Forms.ComboBox();
            this.targetCurrencySelection = new System.Windows.Forms.ComboBox();
            this.convertButton = new System.Windows.Forms.Button();
            this.optionsButton = new System.Windows.Forms.Button();
            this.sourceCurrencyInput = new System.Windows.Forms.TextBox();
            this.targetCurrencyOutput = new System.Windows.Forms.TextBox();
            this.inputValueLabel = new System.Windows.Forms.Label();
            this.outputValueLabel = new System.Windows.Forms.Label();
            this.inputCurrencyLabel = new System.Windows.Forms.Label();
            this.outputCurrencyLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // sourceCurrencySelection
            // 
            this.sourceCurrencySelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sourceCurrencySelection.FormattingEnabled = true;
            this.sourceCurrencySelection.Location = new System.Drawing.Point(225, 63);
            this.sourceCurrencySelection.Name = "sourceCurrencySelection";
            this.sourceCurrencySelection.Size = new System.Drawing.Size(121, 21);
            this.sourceCurrencySelection.TabIndex = 0;
            // 
            // targetCurrencySelection
            // 
            this.targetCurrencySelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.targetCurrencySelection.FormattingEnabled = true;
            this.targetCurrencySelection.Location = new System.Drawing.Point(225, 116);
            this.targetCurrencySelection.Name = "targetCurrencySelection";
            this.targetCurrencySelection.Size = new System.Drawing.Size(121, 21);
            this.targetCurrencySelection.TabIndex = 1;
            // 
            // convertButton
            // 
            this.convertButton.Enabled = false;
            this.convertButton.Location = new System.Drawing.Point(44, 173);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(75, 23);
            this.convertButton.TabIndex = 3;
            this.convertButton.Text = "Konwertuj";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.convertButton_Click);
            // 
            // optionsButton
            // 
            this.optionsButton.Location = new System.Drawing.Point(12, 12);
            this.optionsButton.Name = "optionsButton";
            this.optionsButton.Size = new System.Drawing.Size(48, 23);
            this.optionsButton.TabIndex = 4;
            this.optionsButton.Text = "Opcje";
            this.optionsButton.UseVisualStyleBackColor = true;
            this.optionsButton.Click += new System.EventHandler(this.optionsButton_Click);
            // 
            // sourceCurrencyInput
            // 
            this.sourceCurrencyInput.Location = new System.Drawing.Point(63, 64);
            this.sourceCurrencyInput.Name = "sourceCurrencyInput";
            this.sourceCurrencyInput.Size = new System.Drawing.Size(100, 20);
            this.sourceCurrencyInput.TabIndex = 2;
            this.sourceCurrencyInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sourceCurrencyInput.Leave += new System.EventHandler(this.sourceCurrencyInput_Leave);
            // 
            // targetCurrencyOutput
            // 
            this.targetCurrencyOutput.Enabled = false;
            this.targetCurrencyOutput.Location = new System.Drawing.Point(63, 116);
            this.targetCurrencyOutput.Name = "targetCurrencyOutput";
            this.targetCurrencyOutput.Size = new System.Drawing.Size(100, 20);
            this.targetCurrencyOutput.TabIndex = 5;
            this.targetCurrencyOutput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // inputValueLabel
            // 
            this.inputValueLabel.AutoSize = true;
            this.inputValueLabel.Location = new System.Drawing.Point(60, 44);
            this.inputValueLabel.Name = "inputValueLabel";
            this.inputValueLabel.Size = new System.Drawing.Size(108, 13);
            this.inputValueLabel.TabIndex = 6;
            this.inputValueLabel.Text = "Wartość początkowa";
            // 
            // outputValueLabel
            // 
            this.outputValueLabel.AutoSize = true;
            this.outputValueLabel.Location = new System.Drawing.Point(66, 97);
            this.outputValueLabel.Name = "outputValueLabel";
            this.outputValueLabel.Size = new System.Drawing.Size(96, 13);
            this.outputValueLabel.TabIndex = 7;
            this.outputValueLabel.Text = "Wartość docelowa";
            // 
            // inputCurrencyLabel
            // 
            this.inputCurrencyLabel.AutoSize = true;
            this.inputCurrencyLabel.Location = new System.Drawing.Point(225, 44);
            this.inputCurrencyLabel.Name = "inputCurrencyLabel";
            this.inputCurrencyLabel.Size = new System.Drawing.Size(102, 13);
            this.inputCurrencyLabel.TabIndex = 8;
            this.inputCurrencyLabel.Text = "Waluta początkowa";
            // 
            // outputCurrencyLabel
            // 
            this.outputCurrencyLabel.AutoSize = true;
            this.outputCurrencyLabel.Location = new System.Drawing.Point(228, 97);
            this.outputCurrencyLabel.Name = "outputCurrencyLabel";
            this.outputCurrencyLabel.Size = new System.Drawing.Size(90, 13);
            this.outputCurrencyLabel.TabIndex = 9;
            this.outputCurrencyLabel.Text = "Waluta docelowa";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 226);
            this.Controls.Add(this.outputCurrencyLabel);
            this.Controls.Add(this.inputCurrencyLabel);
            this.Controls.Add(this.outputValueLabel);
            this.Controls.Add(this.inputValueLabel);
            this.Controls.Add(this.targetCurrencyOutput);
            this.Controls.Add(this.sourceCurrencyInput);
            this.Controls.Add(this.optionsButton);
            this.Controls.Add(this.convertButton);
            this.Controls.Add(this.targetCurrencySelection);
            this.Controls.Add(this.sourceCurrencySelection);
            this.Name = "MainWindow";
            this.Text = "Currency Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox sourceCurrencySelection;
        private System.Windows.Forms.ComboBox targetCurrencySelection;
        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.Button optionsButton;
        private System.Windows.Forms.TextBox sourceCurrencyInput;
        private System.Windows.Forms.TextBox targetCurrencyOutput;
        private System.Windows.Forms.Label inputValueLabel;
        private System.Windows.Forms.Label outputValueLabel;
        private System.Windows.Forms.Label inputCurrencyLabel;
        private System.Windows.Forms.Label outputCurrencyLabel;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurrentyConverter
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();

            //Przypisanie jednostek do list rozwijanych            
            sourceCurrencySelection.DataSource = Enum.GetNames(typeof(Currency.CurrencyList));
            targetCurrencySelection.DataSource = Enum.GetNames(typeof(Currency.CurrencyList));
            Currency.UpdateCurrency();
        }

        //Sprawdzanie poprawności formatu wprowadzonej wartości
        private void sourceCurrencyInput_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(sourceCurrencyInput.Text.ToString(),out result);
            
            if(result ==0)
            {
                  MessageBox.Show("Wprowadzana wartość musi być liczbą!","Currency Converter");
                  sourceCurrencyInput.Clear();
                  sourceCurrencyInput.Focus();
                  convertButton.Enabled = false;
            }
            else
            {
                convertButton.Enabled = true;
            }
        }
        
        private void convertButton_Click(object sender, EventArgs e)
        {
            double currencyInputValue = double.Parse(sourceCurrencyInput.Text.ToString());
            double currencyOutputValue = Conversion.ConvertCurrency(currencyInputValue, sourceCurrencySelection.SelectedIndex, targetCurrencySelection.SelectedIndex);
            currencyOutputValue = Math.Round(currencyOutputValue, 2);

            targetCurrencyOutput.Text = currencyOutputValue.ToString();
            
        }

        private void optionsButton_Click(object sender, EventArgs e)
        {
            OptionsWindow optionsform = new OptionsWindow();

            optionsform.Show();
        }
    }
}

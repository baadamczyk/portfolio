﻿namespace CurrentyConverter
{
    partial class OptionsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usdValue = new System.Windows.Forms.TextBox();
            this.eurValue = new System.Windows.Forms.TextBox();
            this.chfValue = new System.Windows.Forms.TextBox();
            this.gpbValue = new System.Windows.Forms.TextBox();
            this.rubValue = new System.Windows.Forms.TextBox();
            this.jpyValue = new System.Windows.Forms.TextBox();
            this.cnyValue = new System.Windows.Forms.TextBox();
            this.nokValue = new System.Windows.Forms.TextBox();
            this.dkkValue = new System.Windows.Forms.TextBox();
            this.czkValue = new System.Windows.Forms.TextBox();
            this.krwValue = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.applyChanges = new System.Windows.Forms.Button();
            this.restoreChanges = new System.Windows.Forms.Button();
            this.edtionEnabledCheck = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // usdValue
            // 
            this.usdValue.Enabled = false;
            this.usdValue.Location = new System.Drawing.Point(92, 17);
            this.usdValue.Name = "usdValue";
            this.usdValue.Size = new System.Drawing.Size(67, 20);
            this.usdValue.TabIndex = 1;
            this.usdValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.usdValue.Leave += new System.EventHandler(this.usdValue_Leave);
            // 
            // eurValue
            // 
            this.eurValue.Enabled = false;
            this.eurValue.Location = new System.Drawing.Point(92, 43);
            this.eurValue.Name = "eurValue";
            this.eurValue.Size = new System.Drawing.Size(67, 20);
            this.eurValue.TabIndex = 2;
            this.eurValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.eurValue.Leave += new System.EventHandler(this.eurValue_Leave);
            // 
            // chfValue
            // 
            this.chfValue.Enabled = false;
            this.chfValue.Location = new System.Drawing.Point(92, 69);
            this.chfValue.Name = "chfValue";
            this.chfValue.Size = new System.Drawing.Size(67, 20);
            this.chfValue.TabIndex = 3;
            this.chfValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chfValue.Leave += new System.EventHandler(this.chfValue_Leave);
            // 
            // gpbValue
            // 
            this.gpbValue.Enabled = false;
            this.gpbValue.Location = new System.Drawing.Point(92, 95);
            this.gpbValue.Name = "gpbValue";
            this.gpbValue.Size = new System.Drawing.Size(67, 20);
            this.gpbValue.TabIndex = 4;
            this.gpbValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gpbValue.Leave += new System.EventHandler(this.gpbValue_Leave);
            // 
            // rubValue
            // 
            this.rubValue.Enabled = false;
            this.rubValue.Location = new System.Drawing.Point(92, 121);
            this.rubValue.Name = "rubValue";
            this.rubValue.Size = new System.Drawing.Size(67, 20);
            this.rubValue.TabIndex = 5;
            this.rubValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.rubValue.Leave += new System.EventHandler(this.rubValue_Leave);
            // 
            // jpyValue
            // 
            this.jpyValue.Enabled = false;
            this.jpyValue.Location = new System.Drawing.Point(92, 147);
            this.jpyValue.Name = "jpyValue";
            this.jpyValue.Size = new System.Drawing.Size(67, 20);
            this.jpyValue.TabIndex = 6;
            this.jpyValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.jpyValue.Leave += new System.EventHandler(this.jpyValue_Leave);
            // 
            // cnyValue
            // 
            this.cnyValue.Enabled = false;
            this.cnyValue.Location = new System.Drawing.Point(92, 173);
            this.cnyValue.Name = "cnyValue";
            this.cnyValue.Size = new System.Drawing.Size(67, 20);
            this.cnyValue.TabIndex = 7;
            this.cnyValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cnyValue.Leave += new System.EventHandler(this.cnyValue_Leave);
            // 
            // nokValue
            // 
            this.nokValue.Enabled = false;
            this.nokValue.Location = new System.Drawing.Point(92, 277);
            this.nokValue.Name = "nokValue";
            this.nokValue.Size = new System.Drawing.Size(67, 20);
            this.nokValue.TabIndex = 8;
            this.nokValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nokValue.Leave += new System.EventHandler(this.nokValue_Leave);
            // 
            // dkkValue
            // 
            this.dkkValue.Enabled = false;
            this.dkkValue.Location = new System.Drawing.Point(92, 251);
            this.dkkValue.Name = "dkkValue";
            this.dkkValue.Size = new System.Drawing.Size(67, 20);
            this.dkkValue.TabIndex = 9;
            this.dkkValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dkkValue.Leave += new System.EventHandler(this.dkkValue_Leave);
            // 
            // czkValue
            // 
            this.czkValue.Enabled = false;
            this.czkValue.Location = new System.Drawing.Point(92, 225);
            this.czkValue.Name = "czkValue";
            this.czkValue.Size = new System.Drawing.Size(67, 20);
            this.czkValue.TabIndex = 10;
            this.czkValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.czkValue.Leave += new System.EventHandler(this.czkValue_Leave);
            // 
            // krwValue
            // 
            this.krwValue.Enabled = false;
            this.krwValue.Location = new System.Drawing.Point(92, 199);
            this.krwValue.Name = "krwValue";
            this.krwValue.Size = new System.Drawing.Size(67, 20);
            this.krwValue.TabIndex = 11;
            this.krwValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.krwValue.Leave += new System.EventHandler(this.krwValue_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "CHF";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "GPB";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "JPY";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(56, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "CNY";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 202);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "KRW";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(58, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "CZK";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(57, 251);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "DKK";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(56, 277);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "NOK";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(56, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "EUR";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(58, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "USD";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(56, 124);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "RUB";
            // 
            // applyChanges
            // 
            this.applyChanges.Location = new System.Drawing.Point(46, 323);
            this.applyChanges.Name = "applyChanges";
            this.applyChanges.Size = new System.Drawing.Size(100, 23);
            this.applyChanges.TabIndex = 24;
            this.applyChanges.Text = "Zatwierdź zmiany";
            this.applyChanges.UseVisualStyleBackColor = true;
            this.applyChanges.Click += new System.EventHandler(this.applyChanges_Click);
            // 
            // restoreChanges
            // 
            this.restoreChanges.Location = new System.Drawing.Point(184, 323);
            this.restoreChanges.Name = "restoreChanges";
            this.restoreChanges.Size = new System.Drawing.Size(75, 23);
            this.restoreChanges.TabIndex = 25;
            this.restoreChanges.Text = "Przywróć";
            this.restoreChanges.UseVisualStyleBackColor = true;
            this.restoreChanges.Click += new System.EventHandler(this.button2_Click);
            // 
            // edtionEnabledCheck
            // 
            this.edtionEnabledCheck.AutoSize = true;
            this.edtionEnabledCheck.Location = new System.Drawing.Point(184, 146);
            this.edtionEnabledCheck.Name = "edtionEnabledCheck";
            this.edtionEnabledCheck.Size = new System.Drawing.Size(58, 17);
            this.edtionEnabledCheck.TabIndex = 26;
            this.edtionEnabledCheck.Text = "Edycja";
            this.edtionEnabledCheck.UseVisualStyleBackColor = true;
            this.edtionEnabledCheck.CheckedChanged += new System.EventHandler(this.editionEnabledCheck_CheckedChanged);
            // 
            // OptionsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(296, 371);
            this.Controls.Add(this.edtionEnabledCheck);
            this.Controls.Add(this.restoreChanges);
            this.Controls.Add(this.applyChanges);
            this.Controls.Add(this.dkkValue);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.nokValue);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.krwValue);
            this.Controls.Add(this.usdValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.czkValue);
            this.Controls.Add(this.eurValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chfValue);
            this.Controls.Add(this.cnyValue);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.gpbValue);
            this.Controls.Add(this.jpyValue);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.rubValue);
            this.Name = "OptionsWindow";
            this.Text = "Edycja kursów dla PLN";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox usdValue;
        private System.Windows.Forms.TextBox eurValue;
        private System.Windows.Forms.TextBox chfValue;
        private System.Windows.Forms.TextBox gpbValue;
        private System.Windows.Forms.TextBox rubValue;
        private System.Windows.Forms.TextBox jpyValue;
        private System.Windows.Forms.TextBox cnyValue;
        private System.Windows.Forms.TextBox nokValue;
        private System.Windows.Forms.TextBox dkkValue;
        private System.Windows.Forms.TextBox czkValue;
        private System.Windows.Forms.TextBox krwValue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button applyChanges;
        private System.Windows.Forms.Button restoreChanges;
        private System.Windows.Forms.CheckBox edtionEnabledCheck;
    }
}
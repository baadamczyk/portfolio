﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CurrentyConverter
{
    public partial class OptionsWindow : Form
    {
        public OptionsWindow()
        {
            InitializeComponent();
            GetFieldValues();

            // !!!!!!!!!!!!!!!!!!!!!W PRZYSZŁOŚCI WPROWADZIĆ MODYFIKACJĘ POBIERAJĄCĄ DANE O KURSACH WALUT Z SERWERA!!!!!!!!!!!!!!!!!!!

        }
                

        private void editionEnabledCheck_CheckedChanged(object sender, EventArgs e)
        {

            if (edtionEnabledCheck.Checked==true)
            {
                usdValue.Enabled = true;
                eurValue.Enabled = true;
                chfValue.Enabled = true;
                gpbValue.Enabled = true;
                rubValue.Enabled = true;
                jpyValue.Enabled = true;
                cnyValue.Enabled = true;
                krwValue.Enabled = true;
                czkValue.Enabled = true;
                dkkValue.Enabled = true;
                nokValue.Enabled = true; 
            }            
            else
            {
                usdValue.Enabled = false;
                eurValue.Enabled = false;
                chfValue.Enabled = false;
                gpbValue.Enabled = false;
                rubValue.Enabled = false;
                jpyValue.Enabled = false;
                cnyValue.Enabled = false;
                krwValue.Enabled = false;
                czkValue.Enabled = false;
                dkkValue.Enabled = false;
                nokValue.Enabled = false;
                GetFieldValues();
            }
        }

        private void GetFieldValues()
        {
            usdValue.Text=Currency.currencyArray[1].ToString();
            eurValue.Text=Currency.currencyArray[2].ToString();
            chfValue.Text=Currency.currencyArray[3].ToString();
            gpbValue.Text=Currency.currencyArray[4].ToString();
            rubValue.Text=Currency.currencyArray[5].ToString();
            jpyValue.Text=Currency.currencyArray[6].ToString();
            cnyValue.Text=Currency.currencyArray[7].ToString();
            krwValue.Text=Currency.currencyArray[8].ToString();
            czkValue.Text=Currency.currencyArray[9].ToString();
            dkkValue.Text=Currency.currencyArray[10].ToString();
            nokValue.Text=Currency.currencyArray[11].ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GetFieldValues();
        }

        private void applyChanges_Click(object sender, EventArgs e)
        {
           ;
           if (MessageBox.Show("Czy na pewno chcesz zastosować zmiany?", "Currency Converter", MessageBoxButtons.OKCancel)==DialogResult.OK)
            {
                SaveToFile();
                Currency.UpdateCurrency();
                GetFieldValues();
            }
        }

        private void usdValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(usdValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                usdValue.Focus();                
            }
        }


        private void eurValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(eurValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                eurValue.Focus();                
            }
        }


        private void chfValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(chfValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                chfValue.Focus();                
            }
        }


        private void gpbValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(gpbValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                gpbValue.Focus();                
            }
        }


        private void rubValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(rubValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                rubValue.Focus();                
            }
        }


        private void jpyValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(jpyValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                jpyValue.Focus();                
            }
        }


        private void cnyValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(cnyValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                cnyValue.Focus();                
            }
        }


        private void krwValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(krwValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                krwValue.Focus();                
            }
        }


        private void czkValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(czkValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                czkValue.Focus();                
            }
        }


        private void dkkValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(dkkValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                dkkValue.Focus();                
            }
        }


        private void nokValue_Leave(object sender, EventArgs e)
        {
            double result;
            double.TryParse(nokValue.Text.ToString(), out result);

            if (result == 0)
            {
                MessageBox.Show("Wprowadzana wartość musi być liczbą!", "Currency Converter");
                Currency.currencyArray[1].ToString();
                nokValue.Focus();                
            }
        }

        private void SaveToFile()
        {
            Currency.currencyArray[1] = double.Parse(usdValue.Text.ToString());
            Currency.currencyArray[2] = double.Parse(eurValue.Text.ToString());
            Currency.currencyArray[3] = double.Parse(chfValue.Text.ToString());
            Currency.currencyArray[4] = double.Parse(gpbValue.Text.ToString());
            Currency.currencyArray[5] = double.Parse(rubValue.Text.ToString());
            Currency.currencyArray[6] = double.Parse(jpyValue.Text.ToString());
            Currency.currencyArray[7] = double.Parse(cnyValue.Text.ToString());
            Currency.currencyArray[8] = double.Parse(krwValue.Text.ToString());
            Currency.currencyArray[9] = double.Parse(czkValue.Text.ToString());
            Currency.currencyArray[10] = double.Parse(dkkValue.Text.ToString());
            Currency.currencyArray[11] = double.Parse(nokValue.Text.ToString());

            string[] stringCurrencyArray = new string[12];
            for(int i=0;i<12;i++)
            {
                stringCurrencyArray[i] = Currency.currencyArray[i].ToString();
                System.IO.File.WriteAllLines(@"db", stringCurrencyArray);
            }
        }
    }
}

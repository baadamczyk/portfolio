**Contents: **

* **ASCIIArtName** - String to ASCII Art converter.
* **BMICalc** - BMI index calculator.
* **CurrencyConverter** - Simple currency converter.
* **StringReverse** - Application for reversing string-type input.
* **CalculateAge** - Application that calculates time (in seconds) elapsed from chosen date.
* **FizzBuzz** - Fizz/buzz problem solution.
* **NameGenerator** - Random names generator.
* **RockPaperScissors** - Rock/paper/scissors-like game.
* **TemperatureConverter** - Temperature unit converter.
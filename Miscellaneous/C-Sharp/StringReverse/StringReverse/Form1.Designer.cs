﻿namespace StringReverse
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputStringTextbox = new System.Windows.Forms.TextBox();
            this.outputStringTextbox = new System.Windows.Forms.TextBox();
            this.inputStringLabel = new System.Windows.Forms.Label();
            this.outputStringLabel = new System.Windows.Forms.Label();
            this.doReverseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // inputStringTextbox
            // 
            this.inputStringTextbox.Location = new System.Drawing.Point(34, 47);
            this.inputStringTextbox.Name = "inputStringTextbox";
            this.inputStringTextbox.Size = new System.Drawing.Size(415, 20);
            this.inputStringTextbox.TabIndex = 0;
            this.inputStringTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // outputStringTextbox
            // 
            this.outputStringTextbox.Enabled = false;
            this.outputStringTextbox.Location = new System.Drawing.Point(34, 108);
            this.outputStringTextbox.Name = "outputStringTextbox";
            this.outputStringTextbox.Size = new System.Drawing.Size(415, 20);
            this.outputStringTextbox.TabIndex = 1;
            this.outputStringTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // inputStringLabel
            // 
            this.inputStringLabel.AutoSize = true;
            this.inputStringLabel.Location = new System.Drawing.Point(197, 19);
            this.inputStringLabel.Name = "inputStringLabel";
            this.inputStringLabel.Size = new System.Drawing.Size(114, 13);
            this.inputStringLabel.TabIndex = 2;
            this.inputStringLabel.Text = "Podaj wejściowy tekst:";
            // 
            // outputStringLabel
            // 
            this.outputStringLabel.AutoSize = true;
            this.outputStringLabel.Location = new System.Drawing.Point(208, 83);
            this.outputStringLabel.Name = "outputStringLabel";
            this.outputStringLabel.Size = new System.Drawing.Size(92, 13);
            this.outputStringLabel.TabIndex = 3;
            this.outputStringLabel.Text = "Tekst odwrócony:";
            // 
            // doReverseButton
            // 
            this.doReverseButton.Location = new System.Drawing.Point(211, 159);
            this.doReverseButton.Name = "doReverseButton";
            this.doReverseButton.Size = new System.Drawing.Size(75, 23);
            this.doReverseButton.TabIndex = 4;
            this.doReverseButton.Text = "Odwróć";
            this.doReverseButton.UseVisualStyleBackColor = true;
            this.doReverseButton.Click += new System.EventHandler(this.doReverseButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 206);
            this.Controls.Add(this.doReverseButton);
            this.Controls.Add(this.outputStringLabel);
            this.Controls.Add(this.inputStringLabel);
            this.Controls.Add(this.outputStringTextbox);
            this.Controls.Add(this.inputStringTextbox);
            this.Name = "Form1";
            this.Text = "String Reverse";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputStringTextbox;
        private System.Windows.Forms.TextBox outputStringTextbox;
        private System.Windows.Forms.Label inputStringLabel;
        private System.Windows.Forms.Label outputStringLabel;
        private System.Windows.Forms.Button doReverseButton;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StringReverse
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void doReverseButton_Click(object sender, EventArgs e)
        {
            Reverse newString = new Reverse();            
            outputStringTextbox.Text = newString.reverseString(inputStringTextbox.Text.ToString());
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringReverse
{
    class Reverse
    {
        char[] processingArray;

        public string reverseString(string inputString)
        {
            processingArray = inputString.ToCharArray();
            Array.Reverse(processingArray);            

            return new string (processingArray);
        }
    }
}

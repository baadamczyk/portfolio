﻿namespace calculateAge
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.birthDateLabel = new System.Windows.Forms.Label();
            this.ageInSecondsLabel = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.ageInSecondsOutput = new System.Windows.Forms.TextBox();
            this.userBirthDatePicker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // birthDateLabel
            // 
            this.birthDateLabel.AutoSize = true;
            this.birthDateLabel.Location = new System.Drawing.Point(67, 24);
            this.birthDateLabel.Name = "birthDateLabel";
            this.birthDateLabel.Size = new System.Drawing.Size(112, 13);
            this.birthDateLabel.TabIndex = 0;
            this.birthDateLabel.Text = "Twoja data urodzenia:";
            // 
            // ageInSecondsLabel
            // 
            this.ageInSecondsLabel.AutoSize = true;
            this.ageInSecondsLabel.Location = new System.Drawing.Point(54, 88);
            this.ageInSecondsLabel.Name = "ageInSecondsLabel";
            this.ageInSecondsLabel.Size = new System.Drawing.Size(125, 13);
            this.ageInSecondsLabel.TabIndex = 1;
            this.ageInSecondsLabel.Text = "Twój wiek w sekundach:";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(74, 163);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 2;
            this.startButton.Text = "POLICZ";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // ageInSecondsOutput
            // 
            this.ageInSecondsOutput.Enabled = false;
            this.ageInSecondsOutput.Location = new System.Drawing.Point(64, 118);
            this.ageInSecondsOutput.Name = "ageInSecondsOutput";
            this.ageInSecondsOutput.Size = new System.Drawing.Size(100, 20);
            this.ageInSecondsOutput.TabIndex = 6;
            this.ageInSecondsOutput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // userBirthDatePicker
            // 
            this.userBirthDatePicker.Cursor = System.Windows.Forms.Cursors.No;
            this.userBirthDatePicker.Location = new System.Drawing.Point(48, 52);
            this.userBirthDatePicker.MaxDate = new System.DateTime(2016, 7, 21, 15, 24, 36, 0);
            this.userBirthDatePicker.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.userBirthDatePicker.Name = "userBirthDatePicker";
            this.userBirthDatePicker.Size = new System.Drawing.Size(131, 20);
            this.userBirthDatePicker.TabIndex = 10;
            this.userBirthDatePicker.Value = new System.DateTime(2016, 7, 21, 0, 0, 0, 0);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 211);
            this.Controls.Add(this.userBirthDatePicker);
            this.Controls.Add(this.ageInSecondsOutput);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.ageInSecondsLabel);
            this.Controls.Add(this.birthDateLabel);
            this.Name = "Form1";
            this.Text = "Age To Seconds";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label birthDateLabel;
        private System.Windows.Forms.Label ageInSecondsLabel;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.TextBox ageInSecondsOutput;
        private System.Windows.Forms.DateTimePicker userBirthDatePicker;
    }
}


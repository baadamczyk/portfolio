﻿using System;
using System.Windows.Forms;

namespace calculateAge
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            userBirthDatePicker.MaxDate = DateTime.Now;                     
        }               

        private void button1_Click(object sender, EventArgs e)
        {
            ageToSeconds();                           
        }

        int ageToSeconds()
        {
            ageInSecondsOutput.Text = CalculateDifference();
            return 0;
        }

        private string CalculateDifference()
        {
            DateTime userBirthDate = userBirthDatePicker.Value;
            DateTime systemDate = DateTime.Now;
            TimeSpan resultDate = systemDate.Subtract(userBirthDate);
            double secondsElpased = resultDate.TotalSeconds;
            double ageInSeconds = Math.Round(secondsElpased);
            return ageInSeconds.ToString();
        }
    }
    
}

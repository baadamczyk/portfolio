﻿namespace headsTails
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chooseHeadsButton = new System.Windows.Forms.Button();
            this.chooseTailsButton = new System.Windows.Forms.Button();
            this.tossAnimation = new System.Windows.Forms.PictureBox();
            this.questionMarkPicture = new System.Windows.Forms.PictureBox();
            this.tailsPicture = new System.Windows.Forms.PictureBox();
            this.headsPicture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.tossAnimation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.questionMarkPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tailsPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headsPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // chooseHeadsButton
            // 
            this.chooseHeadsButton.Location = new System.Drawing.Point(19, 302);
            this.chooseHeadsButton.Name = "chooseHeadsButton";
            this.chooseHeadsButton.Size = new System.Drawing.Size(75, 23);
            this.chooseHeadsButton.TabIndex = 2;
            this.chooseHeadsButton.Text = "ORZEŁ";
            this.chooseHeadsButton.UseVisualStyleBackColor = true;
            this.chooseHeadsButton.Click += new System.EventHandler(this.chooseHeadsButton_Click);
            // 
            // chooseTailsButton
            // 
            this.chooseTailsButton.Location = new System.Drawing.Point(254, 302);
            this.chooseTailsButton.Name = "chooseTailsButton";
            this.chooseTailsButton.Size = new System.Drawing.Size(75, 23);
            this.chooseTailsButton.TabIndex = 3;
            this.chooseTailsButton.Text = "RESZKA";
            this.chooseTailsButton.UseVisualStyleBackColor = true;
            this.chooseTailsButton.Click += new System.EventHandler(this.chooseTailsButton_Click);
            // 
            // tossAnimation
            // 
            this.tossAnimation.Location = new System.Drawing.Point(0, 0);
            this.tossAnimation.Name = "tossAnimation";
            this.tossAnimation.Size = new System.Drawing.Size(100, 50);
            this.tossAnimation.TabIndex = 4;
            this.tossAnimation.TabStop = false;
            // 
            // questionMarkPicture
            // 
            this.questionMarkPicture.Image = global::headsTails.Properties.Resources.questionMark1;
            this.questionMarkPicture.Location = new System.Drawing.Point(49, 26);
            this.questionMarkPicture.Name = "questionMarkPicture";
            this.questionMarkPicture.Size = new System.Drawing.Size(260, 270);
            this.questionMarkPicture.TabIndex = 4;
            this.questionMarkPicture.TabStop = false;
            // 
            // tailsPicture
            // 
            this.tailsPicture.Image = global::headsTails.Properties.Resources.tails;
            this.tailsPicture.Location = new System.Drawing.Point(49, 26);
            this.tailsPicture.Name = "tailsPicture";
            this.tailsPicture.Size = new System.Drawing.Size(260, 270);
            this.tailsPicture.TabIndex = 1;
            this.tailsPicture.TabStop = false;
            this.tailsPicture.Visible = false;
            // 
            // headsPicture
            // 
            this.headsPicture.Image = global::headsTails.Properties.Resources.heads;
            this.headsPicture.Location = new System.Drawing.Point(49, 26);
            this.headsPicture.Name = "headsPicture";
            this.headsPicture.Size = new System.Drawing.Size(260, 270);
            this.headsPicture.TabIndex = 0;
            this.headsPicture.TabStop = false;
            this.headsPicture.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 356);
            this.Controls.Add(this.chooseTailsButton);
            this.Controls.Add(this.chooseHeadsButton);
            this.Controls.Add(this.tossAnimation);
            this.Controls.Add(this.questionMarkPicture);
            this.Controls.Add(this.tailsPicture);
            this.Controls.Add(this.headsPicture);
            this.Name = "Form1";
            this.Text = "Heads/Tails";
            ((System.ComponentModel.ISupportInitialize)(this.tossAnimation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.questionMarkPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tailsPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headsPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox headsPicture;
        private System.Windows.Forms.PictureBox tailsPicture;
        private System.Windows.Forms.Button chooseHeadsButton;
        private System.Windows.Forms.Button chooseTailsButton;
        private System.Windows.Forms.PictureBox questionMarkPicture;
        private System.Windows.Forms.PictureBox tossAnimation;
    }
}


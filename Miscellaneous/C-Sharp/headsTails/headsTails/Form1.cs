﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace headsTails
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();            
        }

        
//Rzut monetą      
        int flipCoin(int playerChoice)
        {
            Random flip = new Random();
            int headOrTailsA = flip.Next(0, 100);
            int headOrTails;
            if (headOrTailsA <= 50) headOrTails = 0; else headOrTails = 1;
            
            switch (headOrTails)
            {
                case 0:                                        
                    questionMarkPicture.Visible = false;                    
                    headsPicture.Visible = true;
                    if (playerChoice == headOrTails) MessageBox.Show("WYGRANA!","Heads/Tails");
                    else MessageBox.Show("PRZEGRANA!");
                    break;
                case 1:
                    questionMarkPicture.Visible = false;
                    tailsPicture.Visible = true;
                    if (playerChoice == headOrTails) MessageBox.Show("WYGRANA!","Heads/Tails");
                    else MessageBox.Show("PRZEGRANA!");
                    break;
                default:
                    break;
            }
            restartGame();
            return 0;
        }
//Nowa gra
        int restartGame()
        {
            if ((MessageBox.Show("JESZCZE RAZ?", "Heads/Tails", MessageBoxButtons.YesNo)) == DialogResult.Yes)
            {
                headsPicture.Visible = false;
                tailsPicture.Visible = false;
                questionMarkPicture.Visible = true;
            }
            else
            {
                MessageBox.Show("ZAMYKAM APLIKACJĘ!", "Heads/Tails");
                Application.Exit();
            }
                
            
            return 0;
        }
//Wybór
        private void chooseHeadsButton_Click(object sender, EventArgs e)
        {
            int playerChoice=0;
            flipCoin(playerChoice);
        }

        private void chooseTailsButton_Click(object sender, EventArgs e)
        {
            int playerChoice = 1;
            flipCoin(playerChoice);
        }
    }
}

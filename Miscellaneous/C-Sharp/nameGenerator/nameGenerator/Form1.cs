﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nameGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
            
        }
//Obsługa zdarzenia klinięcia
        private void generateButton_Click(object sender, EventArgs e)
        {
            int gender;

            if (femaleRadioButton.Checked == true)
            {
                gender = 1;
            }
            else
            {
                gender = 2;
            }

            generateName(gender);
        }

        string generateName(int gender)
        {            
            string[] names = new String[100];
            
            switch (gender)
            {
                case 1:
                    for (int i = 0; i < 100; i++)
                    {
                        names[i] = System.IO.File.ReadLines(@"db\female").Skip(i).Take(1 + i).First();
                    }
                    break;
                case 2:
                    for (int i = 0; i < 100; i++)
                    {
                        names[i] = System.IO.File.ReadLines(@"db\male").Skip(i).Take(1 + i).First();
                    }
                    break;
                default:
                    return null;
            }

            //losowanie imienia (zbiór 100 elementów)
            
            Random random = new Random();
            int nameSelect = random.Next(0,99);
            resultField.Text = names[nameSelect];
            
            
            return null;

        }
    }
    
}

﻿namespace rockPaperScissors
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paperA = new System.Windows.Forms.PictureBox();
            this.paperB = new System.Windows.Forms.PictureBox();
            this.rockA = new System.Windows.Forms.PictureBox();
            this.scissorsB = new System.Windows.Forms.PictureBox();
            this.scissorsA = new System.Windows.Forms.PictureBox();
            this.rockB = new System.Windows.Forms.PictureBox();
            this.chooseRockButton = new System.Windows.Forms.PictureBox();
            this.choosePaperButton = new System.Windows.Forms.PictureBox();
            this.chooseScissorsButton = new System.Windows.Forms.PictureBox();
            this.cpuStats = new System.Windows.Forms.GroupBox();
            this.playerStats = new System.Windows.Forms.GroupBox();
            this.playerWins = new System.Windows.Forms.TextBox();
            this.playerLosses = new System.Windows.Forms.TextBox();
            this.cpuLosses = new System.Windows.Forms.TextBox();
            this.cpuWins = new System.Windows.Forms.TextBox();
            this.playerWinsLabel = new System.Windows.Forms.Label();
            this.playerLossesLabel = new System.Windows.Forms.Label();
            this.cpuLossesLabel = new System.Windows.Forms.Label();
            this.cpuWinsLabel = new System.Windows.Forms.Label();
            this.restartGameButton = new System.Windows.Forms.Button();
            this.initialLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.paperA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rockA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scissorsB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scissorsA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rockB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseRockButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosePaperButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseScissorsButton)).BeginInit();
            this.cpuStats.SuspendLayout();
            this.playerStats.SuspendLayout();
            this.SuspendLayout();
            // 
            // paperA
            // 
            this.paperA.Image = global::rockPaperScissors.Properties.Resources.paper1;
            this.paperA.Location = new System.Drawing.Point(25, 137);
            this.paperA.Name = "paperA";
            this.paperA.Size = new System.Drawing.Size(460, 330);
            this.paperA.TabIndex = 13;
            this.paperA.TabStop = false;
            this.paperA.Visible = false;
            // 
            // paperB
            // 
            this.paperB.Image = global::rockPaperScissors.Properties.Resources.paper1;
            this.paperB.Location = new System.Drawing.Point(728, 137);
            this.paperB.Name = "paperB";
            this.paperB.Size = new System.Drawing.Size(460, 330);
            this.paperB.TabIndex = 12;
            this.paperB.TabStop = false;
            this.paperB.Visible = false;
            // 
            // rockA
            // 
            this.rockA.Image = global::rockPaperScissors.Properties.Resources.rock1;
            this.rockA.Location = new System.Drawing.Point(25, 137);
            this.rockA.Name = "rockA";
            this.rockA.Size = new System.Drawing.Size(530, 330);
            this.rockA.TabIndex = 11;
            this.rockA.TabStop = false;
            this.rockA.Visible = false;
            // 
            // scissorsB
            // 
            this.scissorsB.Image = global::rockPaperScissors.Properties.Resources.scissors1;
            this.scissorsB.Location = new System.Drawing.Point(658, 169);
            this.scissorsB.Name = "scissorsB";
            this.scissorsB.Size = new System.Drawing.Size(530, 290);
            this.scissorsB.TabIndex = 10;
            this.scissorsB.TabStop = false;
            this.scissorsB.Visible = false;
            // 
            // scissorsA
            // 
            this.scissorsA.Image = global::rockPaperScissors.Properties.Resources.scissors1;
            this.scissorsA.Location = new System.Drawing.Point(25, 169);
            this.scissorsA.Name = "scissorsA";
            this.scissorsA.Size = new System.Drawing.Size(530, 290);
            this.scissorsA.TabIndex = 5;
            this.scissorsA.TabStop = false;
            this.scissorsA.Visible = false;
            // 
            // rockB
            // 
            this.rockB.Image = global::rockPaperScissors.Properties.Resources.rock1;
            this.rockB.Location = new System.Drawing.Point(654, 137);
            this.rockB.Name = "rockB";
            this.rockB.Size = new System.Drawing.Size(530, 330);
            this.rockB.TabIndex = 4;
            this.rockB.TabStop = false;
            this.rockB.Visible = false;
            // 
            // chooseRockButton
            // 
            this.chooseRockButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.chooseRockButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chooseRockButton.Image = global::rockPaperScissors.Properties.Resources.miniRock;
            this.chooseRockButton.Location = new System.Drawing.Point(142, 499);
            this.chooseRockButton.Name = "chooseRockButton";
            this.chooseRockButton.Size = new System.Drawing.Size(64, 102);
            this.chooseRockButton.TabIndex = 3;
            this.chooseRockButton.TabStop = false;
            this.chooseRockButton.Click += new System.EventHandler(this.chooseRockButton_Click);
            // 
            // choosePaperButton
            // 
            this.choosePaperButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.choosePaperButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.choosePaperButton.Image = global::rockPaperScissors.Properties.Resources.miniPaper;
            this.choosePaperButton.Location = new System.Drawing.Point(570, 501);
            this.choosePaperButton.Name = "choosePaperButton";
            this.choosePaperButton.Size = new System.Drawing.Size(78, 100);
            this.choosePaperButton.TabIndex = 2;
            this.choosePaperButton.TabStop = false;
            this.choosePaperButton.Click += new System.EventHandler(this.choosePaperButton_Click);
            // 
            // chooseScissorsButton
            // 
            this.chooseScissorsButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.chooseScissorsButton.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chooseScissorsButton.Image = global::rockPaperScissors.Properties.Resources.miniScissors;
            this.chooseScissorsButton.Location = new System.Drawing.Point(980, 501);
            this.chooseScissorsButton.Name = "chooseScissorsButton";
            this.chooseScissorsButton.Size = new System.Drawing.Size(58, 100);
            this.chooseScissorsButton.TabIndex = 1;
            this.chooseScissorsButton.TabStop = false;
            this.chooseScissorsButton.Click += new System.EventHandler(this.chooseScissorsButton_Click);
            // 
            // cpuStats
            // 
            this.cpuStats.Controls.Add(this.cpuWinsLabel);
            this.cpuStats.Controls.Add(this.cpuLossesLabel);
            this.cpuStats.Controls.Add(this.cpuWins);
            this.cpuStats.Controls.Add(this.cpuLosses);
            this.cpuStats.Enabled = false;
            this.cpuStats.Location = new System.Drawing.Point(838, 12);
            this.cpuStats.Name = "cpuStats";
            this.cpuStats.Size = new System.Drawing.Size(200, 100);
            this.cpuStats.TabIndex = 14;
            this.cpuStats.TabStop = false;
            this.cpuStats.Text = "CPU";
            // 
            // playerStats
            // 
            this.playerStats.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.playerStats.Controls.Add(this.playerLossesLabel);
            this.playerStats.Controls.Add(this.playerWinsLabel);
            this.playerStats.Controls.Add(this.playerLosses);
            this.playerStats.Controls.Add(this.playerWins);
            this.playerStats.Location = new System.Drawing.Point(142, 12);
            this.playerStats.Name = "playerStats";
            this.playerStats.Size = new System.Drawing.Size(200, 100);
            this.playerStats.TabIndex = 15;
            this.playerStats.TabStop = false;
            this.playerStats.Text = "GRACZ";
            // 
            // playerWins
            // 
            this.playerWins.Enabled = false;
            this.playerWins.Location = new System.Drawing.Point(32, 48);
            this.playerWins.Name = "playerWins";
            this.playerWins.Size = new System.Drawing.Size(45, 20);
            this.playerWins.TabIndex = 0;
            this.playerWins.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // playerLosses
            // 
            this.playerLosses.Enabled = false;
            this.playerLosses.Location = new System.Drawing.Point(126, 48);
            this.playerLosses.Name = "playerLosses";
            this.playerLosses.Size = new System.Drawing.Size(45, 20);
            this.playerLosses.TabIndex = 1;
            this.playerLosses.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cpuLosses
            // 
            this.cpuLosses.Enabled = false;
            this.cpuLosses.Location = new System.Drawing.Point(124, 48);
            this.cpuLosses.Name = "cpuLosses";
            this.cpuLosses.Size = new System.Drawing.Size(45, 20);
            this.cpuLosses.TabIndex = 1;
            this.cpuLosses.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cpuWins
            // 
            this.cpuWins.Enabled = false;
            this.cpuWins.Location = new System.Drawing.Point(33, 48);
            this.cpuWins.Name = "cpuWins";
            this.cpuWins.Size = new System.Drawing.Size(45, 20);
            this.cpuWins.TabIndex = 2;
            this.cpuWins.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // playerWinsLabel
            // 
            this.playerWinsLabel.AutoSize = true;
            this.playerWinsLabel.Location = new System.Drawing.Point(29, 24);
            this.playerWinsLabel.Name = "playerWinsLabel";
            this.playerWinsLabel.Size = new System.Drawing.Size(50, 13);
            this.playerWinsLabel.TabIndex = 2;
            this.playerWinsLabel.Text = "Wygrane";
            // 
            // playerLossesLabel
            // 
            this.playerLossesLabel.AutoSize = true;
            this.playerLossesLabel.Location = new System.Drawing.Point(123, 24);
            this.playerLossesLabel.Name = "playerLossesLabel";
            this.playerLossesLabel.Size = new System.Drawing.Size(55, 13);
            this.playerLossesLabel.TabIndex = 3;
            this.playerLossesLabel.Text = "Przegrane";
            // 
            // cpuLossesLabel
            // 
            this.cpuLossesLabel.AutoSize = true;
            this.cpuLossesLabel.Location = new System.Drawing.Point(121, 24);
            this.cpuLossesLabel.Name = "cpuLossesLabel";
            this.cpuLossesLabel.Size = new System.Drawing.Size(55, 13);
            this.cpuLossesLabel.TabIndex = 4;
            this.cpuLossesLabel.Text = "Przegrane";
            // 
            // cpuWinsLabel
            // 
            this.cpuWinsLabel.AutoSize = true;
            this.cpuWinsLabel.Location = new System.Drawing.Point(30, 24);
            this.cpuWinsLabel.Name = "cpuWinsLabel";
            this.cpuWinsLabel.Size = new System.Drawing.Size(50, 13);
            this.cpuWinsLabel.TabIndex = 5;
            this.cpuWinsLabel.Text = "Wygrane";
            // 
            // restartGameButton
            // 
            this.restartGameButton.Location = new System.Drawing.Point(1113, 12);
            this.restartGameButton.Name = "restartGameButton";
            this.restartGameButton.Size = new System.Drawing.Size(75, 23);
            this.restartGameButton.TabIndex = 16;
            this.restartGameButton.Text = "Restart";
            this.restartGameButton.UseVisualStyleBackColor = true;
            this.restartGameButton.Click += new System.EventHandler(this.restartGameButton_Click);
            // 
            // initialLabel
            // 
            this.initialLabel.AutoSize = true;
            this.initialLabel.Font = new System.Drawing.Font("Arial Black", 72F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initialLabel.Location = new System.Drawing.Point(89, 233);
            this.initialLabel.Name = "initialLabel";
            this.initialLabel.Size = new System.Drawing.Size(1039, 136);
            this.initialLabel.TabIndex = 17;
            this.initialLabel.Text = "WYBIERZ MĄDRZE";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 613);
            this.Controls.Add(this.initialLabel);
            this.Controls.Add(this.restartGameButton);
            this.Controls.Add(this.playerStats);
            this.Controls.Add(this.cpuStats);
            this.Controls.Add(this.chooseRockButton);
            this.Controls.Add(this.choosePaperButton);
            this.Controls.Add(this.chooseScissorsButton);
            this.Controls.Add(this.scissorsA);
            this.Controls.Add(this.scissorsB);
            this.Controls.Add(this.rockA);
            this.Controls.Add(this.rockB);
            this.Controls.Add(this.paperA);
            this.Controls.Add(this.paperB);
            this.Name = "Form1";
            this.Text = "Rock Paper Scissors";
            ((System.ComponentModel.ISupportInitialize)(this.paperA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paperB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rockA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scissorsB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scissorsA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rockB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseRockButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosePaperButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseScissorsButton)).EndInit();
            this.cpuStats.ResumeLayout(false);
            this.cpuStats.PerformLayout();
            this.playerStats.ResumeLayout(false);
            this.playerStats.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox chooseScissorsButton;
        private System.Windows.Forms.PictureBox choosePaperButton;
        private System.Windows.Forms.PictureBox chooseRockButton;
        private System.Windows.Forms.PictureBox rockB;
        private System.Windows.Forms.PictureBox scissorsA;
        private System.Windows.Forms.PictureBox scissorsB;
        private System.Windows.Forms.PictureBox rockA;
        private System.Windows.Forms.PictureBox paperB;
        private System.Windows.Forms.PictureBox paperA;
        private System.Windows.Forms.GroupBox cpuStats;
        private System.Windows.Forms.GroupBox playerStats;
        private System.Windows.Forms.Label cpuWinsLabel;
        private System.Windows.Forms.Label cpuLossesLabel;
        private System.Windows.Forms.TextBox cpuWins;
        private System.Windows.Forms.TextBox cpuLosses;
        private System.Windows.Forms.Label playerLossesLabel;
        private System.Windows.Forms.Label playerWinsLabel;
        private System.Windows.Forms.TextBox playerLosses;
        private System.Windows.Forms.TextBox playerWins;
        private System.Windows.Forms.Button restartGameButton;
        private System.Windows.Forms.Label initialLabel;
    }
}


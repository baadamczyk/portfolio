﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rockPaperScissors
{
    public partial class Form1 : Form
    {
        public int playerChoice = 0;
        public int[,] scores = new int[2, 2]; 
        public Form1()
        {
            InitializeComponent();
            scoresReset();
            scoresUpdate();

            rockB.Image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            paperB.Image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            scissorsB.Image.RotateFlip(RotateFlipType.RotateNoneFlipX);
        }

        void scoresUpdate()
        {
            playerWins.Text = scores[0, 0].ToString();
            playerLosses.Text = scores[1, 0].ToString();
            cpuWins.Text = scores[0, 1].ToString();
            cpuLosses.Text = scores[1, 1].ToString();
        }
        
        void scoresReset()
        {
            for(int i=0;i<=1;i++)
            {
               for(int j=0;j<=1;j++)
               {
                   scores[i,j]=0;
               }
            }
        }

        void game(int playerChoice)
        {
            int cpuChoice;
            
            //Wybór zagrania komputera
            Random random = new Random();
            int randomChoice = random.Next(1,150);

            if(randomChoice <= 50)
            {
                cpuChoice = 1;
            }
            else if(randomChoice <= 100)
            {
                cpuChoice = 2;
            }
            else
            {
                cpuChoice = 3;
            }
            
            //Porównanie decyzji gracza i cpu
            switch(cpuChoice)
            {
                case 1:                    
                    rockB.Visible=true;
                    
                    switch(playerChoice)
                    {
                        case 1:
                            initialLabel.Visible = false;
                            rockA.Visible = true;
                            MessageBox.Show("REMIS!", "Rock Paper Scissors");                            
                            break;
                        case 2:
                            initialLabel.Visible = false;
                            paperA.Visible = true;
                            scores[0, 0]++;
                            scores[1, 1]++;
                            MessageBox.Show("GRACZ ZWYCIĘŻA!", "Rock Paper Scissors");
                            break;
                        case 3:
                            initialLabel.Visible = false;
                            scissorsA.Visible = true;
                            scores[1, 0]++;
                            scores[0, 1]++;
                            MessageBox.Show("KOMPUTER ZWYCIĘŻA!", "Rock Paper Scissors");
                            break;
                        default:
                            break;
                    }                                      
                    break;

                case 2:                    
                    paperB.Visible=true;

                    switch (playerChoice)
                    {
                        case 1:
                            initialLabel.Visible = false;
                            rockA.Visible = true;
                            scores[1, 0]++;
                            scores[0, 1]++;
                            MessageBox.Show("KOMPUTER ZWYCIĘŻA!", "Rock Paper Scissors");
                            break;
                        case 2:
                            initialLabel.Visible = false;
                            paperA.Visible = true;
                            MessageBox.Show("REMIS!", "Rock Paper Scissors");                            
                            break;
                        case 3:
                            initialLabel.Visible = false;
                            scissorsA.Visible = true;
                            scores[0, 0]++;
                            scores[1, 1]++;
                            MessageBox.Show("GRACZ ZWYCIĘŻA!", "Rock Paper Scissors");
                            break;
                        default:
                            break;
                    }
                   
                    break;
                case 3:                    
                    scissorsB.Visible=true;

                    switch (playerChoice)
                    {
                        case 1:
                            initialLabel.Visible = false;
                            rockA.Visible = true;
                            scores[0, 0]++;
                            scores[1, 1]++;
                            MessageBox.Show("GRACZ ZWYCIĘŻA!", "Rock Paper Scissors");                            
                            break;
                        case 2:
                            initialLabel.Visible = false;
                            paperA.Visible = true;
                            scores[1, 0]++;
                            scores[0, 1]++;
                            MessageBox.Show("KOMPUTER ZWYCIĘŻA!", "Rock Paper Scissors");
                            break;
                        case 3:
                            initialLabel.Visible = false;
                            scissorsA.Visible = true;
                            MessageBox.Show("REMIS!", "Rock Paper Scissors");                           
                            break;
                        default:
                            break;
                    }

                    break;
                default:
                    break;
            }

            scoresUpdate();
            rockA.Visible=false;
            rockB.Visible=false;
            paperA.Visible=false;
            paperB.Visible=false;
            scissorsA.Visible=false;
            scissorsB.Visible=false;
            initialLabel.Visible=true;
        }

        private void chooseRockButton_Click(object sender, EventArgs e)
        {
            playerChoice = 1;
            game(playerChoice);
        }

        private void choosePaperButton_Click(object sender, EventArgs e)
        {
            playerChoice = 2;
            game(playerChoice);
        }

        private void chooseScissorsButton_Click(object sender, EventArgs e)
        {
            playerChoice = 3;
            game(playerChoice);
        }

        private void restartGameButton_Click(object sender, EventArgs e)
        {
            scoresReset();
            scoresUpdate();
        }
    }
}

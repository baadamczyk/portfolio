﻿namespace temperatureConverter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.convertButton = new System.Windows.Forms.Button();
            this.unitComboB = new System.Windows.Forms.ComboBox();
            this.unitOutput = new System.Windows.Forms.TextBox();
            this.unitInput = new System.Windows.Forms.TextBox();
            this.unitComboA = new System.Windows.Forms.ComboBox();
            this.labelUnitA = new System.Windows.Forms.Label();
            this.labelOutput = new System.Windows.Forms.Label();
            this.labelUnitB = new System.Windows.Forms.Label();
            this.labelInput = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // convertButton
            // 
            this.convertButton.Location = new System.Drawing.Point(110, 189);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(75, 23);
            this.convertButton.TabIndex = 0;
            this.convertButton.Text = "PRZELICZ";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.convertButton_Click);
            // 
            // unitComboB
            // 
            this.unitComboB.FormattingEnabled = true;
            this.unitComboB.Items.AddRange(new object[] {
            "Celsjusz [*C]",
            "Fahrenheit [*F]",
            "Kelvin [*K]"});
            this.unitComboB.Location = new System.Drawing.Point(130, 109);
            this.unitComboB.Name = "unitComboB";
            this.unitComboB.Size = new System.Drawing.Size(121, 21);
            this.unitComboB.TabIndex = 1;
            // 
            // unitOutput
            // 
            this.unitOutput.Enabled = false;
            this.unitOutput.Location = new System.Drawing.Point(130, 146);
            this.unitOutput.Name = "unitOutput";
            this.unitOutput.Size = new System.Drawing.Size(121, 20);
            this.unitOutput.TabIndex = 2;
            this.unitOutput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // unitInput
            // 
            this.unitInput.Location = new System.Drawing.Point(130, 57);
            this.unitInput.Name = "unitInput";
            this.unitInput.Size = new System.Drawing.Size(121, 20);
            this.unitInput.TabIndex = 4;            
            // 
            // unitComboA
            // 
            this.unitComboA.FormattingEnabled = true;
            this.unitComboA.Items.AddRange(new object[] {
            "Celsjusz [*C]",
            "Fahrenheit [*F]",
            "Kelvin [*K]"});
            this.unitComboA.Location = new System.Drawing.Point(130, 20);
            this.unitComboA.Name = "unitComboA";
            this.unitComboA.Size = new System.Drawing.Size(121, 21);
            this.unitComboA.TabIndex = 3;
            // 
            // labelUnitA
            // 
            this.labelUnitA.AutoSize = true;
            this.labelUnitA.Location = new System.Drawing.Point(17, 28);
            this.labelUnitA.Name = "labelUnitA";
            this.labelUnitA.Size = new System.Drawing.Size(108, 13);
            this.labelUnitA.TabIndex = 5;
            this.labelUnitA.Text = "Jednostka wejściowa";
            // 
            // labelOutput
            // 
            this.labelOutput.AutoSize = true;
            this.labelOutput.Location = new System.Drawing.Point(77, 153);
            this.labelOutput.Name = "labelOutput";
            this.labelOutput.Size = new System.Drawing.Size(47, 13);
            this.labelOutput.TabIndex = 6;
            this.labelOutput.Text = "Wartość";
            // 
            // labelUnitB
            // 
            this.labelUnitB.AutoSize = true;
            this.labelUnitB.Location = new System.Drawing.Point(17, 112);
            this.labelUnitB.Name = "labelUnitB";
            this.labelUnitB.Size = new System.Drawing.Size(107, 13);
            this.labelUnitB.TabIndex = 7;
            this.labelUnitB.Text = "Jednostka wyjściowa";
            // 
            // labelInput
            // 
            this.labelInput.AutoSize = true;
            this.labelInput.Location = new System.Drawing.Point(77, 64);
            this.labelInput.Name = "labelInput";
            this.labelInput.Size = new System.Drawing.Size(47, 13);
            this.labelInput.TabIndex = 8;
            this.labelInput.Text = "Wartość";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 245);
            this.Controls.Add(this.labelInput);
            this.Controls.Add(this.labelUnitB);
            this.Controls.Add(this.labelOutput);
            this.Controls.Add(this.labelUnitA);
            this.Controls.Add(this.unitInput);
            this.Controls.Add(this.unitComboA);
            this.Controls.Add(this.unitOutput);
            this.Controls.Add(this.unitComboB);
            this.Controls.Add(this.convertButton);
            this.Name = "Form1";
            this.Text = "Temperature Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.ComboBox unitComboB;
        private System.Windows.Forms.TextBox unitOutput;
        private System.Windows.Forms.TextBox unitInput;
        private System.Windows.Forms.ComboBox unitComboA;
        private System.Windows.Forms.Label labelUnitA;
        private System.Windows.Forms.Label labelOutput;
        private System.Windows.Forms.Label labelUnitB;
        private System.Windows.Forms.Label labelInput;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace temperatureConverter
{    
    public partial class Form1 : Form
    {
        public double inputValue;
        public Form1()
        {
            InitializeComponent();
            unitComboA.SelectedIndex = 0;
            unitComboB.SelectedIndex = 0;
            
        }

        double IsNumeric()
        {
            double result;
            string input = unitInput.Text;
            double.TryParse(input, out result);
            if (result == 0)
            {
                unitInput.Clear();
                unitOutput.Clear();
                MessageBox.Show("Wymagana jest wartość liczbowa!");
                return 1;
            }
            else
            {
                inputValue=result;
            }

            return 0;                                                               
        }
        int conversionMethod()
        {
            switch(unitComboA.SelectedIndex)
            {
                case 0:
                    switch(unitComboB.SelectedIndex)
                    {
                        case 0:
                            return 1;
                            break;
                        case 1:
                            return 2;
                            break;
                        case 2:
                            return 3;
                            break;
                        default:
                            return 0;
                    }
                    break;
                case 1:
                    switch (unitComboB.SelectedIndex)
                    {
                        case 0:
                            return 4;
                            break;
                        case 1:
                            return 5;
                            break;
                        case 2:
                            return 6;
                            break;
                        default:
                            return 0;
                    }
                    break;
                case 2:
                    switch (unitComboB.SelectedIndex)
                    {
                        case 0:
                            return 7;
                            break;
                        case 1:
                            return 8;
                            break;
                        case 2:
                            return 9;
                            break;
                        default:
                            return 0;
                    }
                    break;
                default:
                    return 0;
            }

            return 0;
        }
        
        
        double convertTemperature(double inputValue, int conversionMethod)
        {
            switch(conversionMethod)
            {
                case 1:
                    return inputValue;
                    break;
                case 2:
                    return ((inputValue*1.8)+32);
                    break;
                case 3:
                    return (inputValue+273.15);
                    break;
                case 4:
                    return ((inputValue-32)/1.8);
                    break;
                case 5:
                    return (inputValue);
                    break;
                case 6:
                    return ((inputValue+459.67)*(5/9));
                    break;
                case 7:
                    return (inputValue-273.15);
                    break;
                case 8:
                    return ((inputValue*1.8)-459.67);
                    break;
                case 9:
                    return (inputValue);
                    break;
                default:
                    return 0;
            }
            
            return 0;
        }
        


        private void convertButton_Click(object sender, EventArgs e)
        {
            unitOutput.Clear();
            if(IsNumeric()==0)
            {                
                double outputValue = convertTemperature(inputValue, conversionMethod());
                unitOutput.Text = outputValue.ToString();
            }


        }        
    }
}

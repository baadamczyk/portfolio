This repository contains a number of sample projects made by Bartłomiej Adamczyk.

Contents:

* **SOAP_LOCAL_ORDER (C#)** - Application for creating a new product order (based on SOAP protocol), which also implements functionality of reciept printing (using POSNET Thermal libraries)
* **Fast Food (C#)** - Simple game where player's goal is to catch the running Cat.
* **Calculator (C#)** - Basic calculator.
* **Snake (C#)** - Snake-type game.

* **Miscellaneous (various languages)** - Minor projects that were done during the process of learning new skills.
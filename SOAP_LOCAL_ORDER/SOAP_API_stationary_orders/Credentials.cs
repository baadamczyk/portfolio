﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOAP_API_stationary_orders
{
    class Credentials
    {
        public static string login { get; set; }
        public static string password { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOAP_API_stationary_orders
{
    class Delivery
    {
        public bool SameAsCustomer { get; set; }

        public string Name { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PaymentMethod { get; set; }
        public string DeliveryMethod { get; set; }

        public Delivery()
        {
            SameAsCustomer = false;
        }
    }
}

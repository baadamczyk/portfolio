﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOAP_API_stationary_orders
{
    class Invoice
    {
        public bool IsRequested { get; set; }
        public bool SameAsCustomer { get; set; }

        public string Name { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public Invoice()
        {
            IsRequested = true;
            SameAsCustomer = false;
        }
    }
}

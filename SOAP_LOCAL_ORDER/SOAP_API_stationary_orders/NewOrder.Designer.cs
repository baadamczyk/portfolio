﻿namespace SOAP_API_stationary_orders
{
    partial class NewOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.orderGroupbox = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.OrderNumberTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.OrderIDTextbox = new System.Windows.Forms.TextBox();
            this.customerGroupbox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CustomerEmailTextbox = new System.Windows.Forms.TextBox();
            this.CustomerPostcodeTextbox = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.CustomerSurnameTextbox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.CustomerCityTextbox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.CustomerAddressTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CustomerNameTextbox = new System.Windows.Forms.TextBox();
            this.invoiceGroupbox = new System.Windows.Forms.GroupBox();
            this.InvoicePostcodeTextbox = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.InvoiceSurnameTextbox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.InvoiceCityTextbox = new System.Windows.Forms.TextBox();
            this.InvoiceCopyCustDataCheck = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.InvoiceAddressTextbox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.InvoiceNameTextbox = new System.Windows.Forms.TextBox();
            this.itemsGroupbox = new System.Windows.Forms.GroupBox();
            this.ItemQuantityTextbox = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ItemIDTextbox = new System.Windows.Forms.MaskedTextBox();
            this.DeleteItemButton = new System.Windows.Forms.Button();
            this.AddItemButton = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.OrderedItemsListbox = new System.Windows.Forms.ListBox();
            this.ProceedButton = new System.Windows.Forms.Button();
            this.CancelForm = new System.Windows.Forms.Button();
            this.DeliveryNameTextbox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.DeliveryAddressTextbox = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.DeliveryCityTextbox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.DeliverySurnameTextbox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.DeliveryCopyCustDataCheck = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.PaymentMethodCombobox = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.DeliveryMethodCombobox = new System.Windows.Forms.ComboBox();
            this.deliveryGroupbox = new System.Windows.Forms.GroupBox();
            this.DeliveryPostcodeTextbox = new System.Windows.Forms.MaskedTextBox();
            this.PrintRecieptsButton = new System.Windows.Forms.Button();
            this.orderGroupbox.SuspendLayout();
            this.customerGroupbox.SuspendLayout();
            this.invoiceGroupbox.SuspendLayout();
            this.itemsGroupbox.SuspendLayout();
            this.deliveryGroupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // orderGroupbox
            // 
            this.orderGroupbox.Controls.Add(this.dateTimePicker1);
            this.orderGroupbox.Controls.Add(this.label4);
            this.orderGroupbox.Controls.Add(this.label3);
            this.orderGroupbox.Controls.Add(this.OrderNumberTextbox);
            this.orderGroupbox.Controls.Add(this.label2);
            this.orderGroupbox.Controls.Add(this.OrderIDTextbox);
            this.orderGroupbox.Enabled = false;
            this.orderGroupbox.Location = new System.Drawing.Point(158, 33);
            this.orderGroupbox.Name = "orderGroupbox";
            this.orderGroupbox.Size = new System.Drawing.Size(569, 51);
            this.orderGroupbox.TabIndex = 0;
            this.orderGroupbox.TabStop = false;
            this.orderGroupbox.Text = "ID/Numer Zamówienia";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(391, 19);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(150, 20);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(355, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Data";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(139, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Numer";
            // 
            // OrderNumberTextbox
            // 
            this.OrderNumberTextbox.Location = new System.Drawing.Point(183, 19);
            this.OrderNumberTextbox.Name = "OrderNumberTextbox";
            this.OrderNumberTextbox.Size = new System.Drawing.Size(150, 20);
            this.OrderNumberTextbox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "ID";
            // 
            // OrderIDTextbox
            // 
            this.OrderIDTextbox.Location = new System.Drawing.Point(43, 19);
            this.OrderIDTextbox.Name = "OrderIDTextbox";
            this.OrderIDTextbox.Size = new System.Drawing.Size(76, 20);
            this.OrderIDTextbox.TabIndex = 2;
            // 
            // customerGroupbox
            // 
            this.customerGroupbox.Controls.Add(this.label1);
            this.customerGroupbox.Controls.Add(this.CustomerEmailTextbox);
            this.customerGroupbox.Controls.Add(this.CustomerPostcodeTextbox);
            this.customerGroupbox.Controls.Add(this.label19);
            this.customerGroupbox.Controls.Add(this.CustomerSurnameTextbox);
            this.customerGroupbox.Controls.Add(this.label14);
            this.customerGroupbox.Controls.Add(this.CustomerCityTextbox);
            this.customerGroupbox.Controls.Add(this.label13);
            this.customerGroupbox.Controls.Add(this.label12);
            this.customerGroupbox.Controls.Add(this.CustomerAddressTextbox);
            this.customerGroupbox.Controls.Add(this.label5);
            this.customerGroupbox.Controls.Add(this.CustomerNameTextbox);
            this.customerGroupbox.Location = new System.Drawing.Point(12, 104);
            this.customerGroupbox.Name = "customerGroupbox";
            this.customerGroupbox.Size = new System.Drawing.Size(268, 201);
            this.customerGroupbox.TabIndex = 1;
            this.customerGroupbox.TabStop = false;
            this.customerGroupbox.Text = "Informacje o kontrahencie";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Email";
            // 
            // CustomerEmailTextbox
            // 
            this.CustomerEmailTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.CustomerEmailTextbox.Location = new System.Drawing.Point(90, 159);
            this.CustomerEmailTextbox.Name = "CustomerEmailTextbox";
            this.CustomerEmailTextbox.Size = new System.Drawing.Size(150, 20);
            this.CustomerEmailTextbox.TabIndex = 18;
            this.CustomerEmailTextbox.Leave += new System.EventHandler(this.CustomerEmailTextbox_Leave);
            // 
            // CustomerPostcodeTextbox
            // 
            this.CustomerPostcodeTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.CustomerPostcodeTextbox.Location = new System.Drawing.Point(90, 107);
            this.CustomerPostcodeTextbox.Mask = "00-000";
            this.CustomerPostcodeTextbox.Name = "CustomerPostcodeTextbox";
            this.CustomerPostcodeTextbox.PromptChar = ' ';
            this.CustomerPostcodeTextbox.ResetOnSpace = false;
            this.CustomerPostcodeTextbox.Size = new System.Drawing.Size(150, 20);
            this.CustomerPostcodeTextbox.TabIndex = 4;
            this.CustomerPostcodeTextbox.Leave += new System.EventHandler(this.CustomerPostcodeTextbox_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 58);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Nazwisko";
            // 
            // CustomerSurnameTextbox
            // 
            this.CustomerSurnameTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.CustomerSurnameTextbox.Location = new System.Drawing.Point(90, 55);
            this.CustomerSurnameTextbox.Name = "CustomerSurnameTextbox";
            this.CustomerSurnameTextbox.Size = new System.Drawing.Size(150, 20);
            this.CustomerSurnameTextbox.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(43, 136);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Miasto";
            // 
            // CustomerCityTextbox
            // 
            this.CustomerCityTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.CustomerCityTextbox.Location = new System.Drawing.Point(90, 133);
            this.CustomerCityTextbox.Name = "CustomerCityTextbox";
            this.CustomerCityTextbox.Size = new System.Drawing.Size(150, 20);
            this.CustomerCityTextbox.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 110);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Kod pocztowy";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(49, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Adres";
            // 
            // CustomerAddressTextbox
            // 
            this.CustomerAddressTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.CustomerAddressTextbox.Location = new System.Drawing.Point(90, 81);
            this.CustomerAddressTextbox.Name = "CustomerAddressTextbox";
            this.CustomerAddressTextbox.Size = new System.Drawing.Size(150, 20);
            this.CustomerAddressTextbox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Imię";
            // 
            // CustomerNameTextbox
            // 
            this.CustomerNameTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.CustomerNameTextbox.Location = new System.Drawing.Point(90, 27);
            this.CustomerNameTextbox.Name = "CustomerNameTextbox";
            this.CustomerNameTextbox.Size = new System.Drawing.Size(150, 20);
            this.CustomerNameTextbox.TabIndex = 1;
            // 
            // invoiceGroupbox
            // 
            this.invoiceGroupbox.Controls.Add(this.InvoicePostcodeTextbox);
            this.invoiceGroupbox.Controls.Add(this.label8);
            this.invoiceGroupbox.Controls.Add(this.InvoiceSurnameTextbox);
            this.invoiceGroupbox.Controls.Add(this.label10);
            this.invoiceGroupbox.Controls.Add(this.InvoiceCityTextbox);
            this.invoiceGroupbox.Controls.Add(this.InvoiceCopyCustDataCheck);
            this.invoiceGroupbox.Controls.Add(this.label11);
            this.invoiceGroupbox.Controls.Add(this.label16);
            this.invoiceGroupbox.Controls.Add(this.InvoiceAddressTextbox);
            this.invoiceGroupbox.Controls.Add(this.label28);
            this.invoiceGroupbox.Controls.Add(this.InvoiceNameTextbox);
            this.invoiceGroupbox.Location = new System.Drawing.Point(312, 104);
            this.invoiceGroupbox.Name = "invoiceGroupbox";
            this.invoiceGroupbox.Size = new System.Drawing.Size(268, 201);
            this.invoiceGroupbox.TabIndex = 5;
            this.invoiceGroupbox.TabStop = false;
            this.invoiceGroupbox.Text = "Dane do faktury";
            // 
            // InvoicePostcodeTextbox
            // 
            this.InvoicePostcodeTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.InvoicePostcodeTextbox.Location = new System.Drawing.Point(89, 109);
            this.InvoicePostcodeTextbox.Mask = "00-000";
            this.InvoicePostcodeTextbox.Name = "InvoicePostcodeTextbox";
            this.InvoicePostcodeTextbox.PromptChar = ' ';
            this.InvoicePostcodeTextbox.ResetOnSpace = false;
            this.InvoicePostcodeTextbox.Size = new System.Drawing.Size(150, 20);
            this.InvoicePostcodeTextbox.TabIndex = 9;
            this.InvoicePostcodeTextbox.Leave += new System.EventHandler(this.InvoicePostcodeTextbox_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Nazwisko";
            // 
            // InvoiceSurnameTextbox
            // 
            this.InvoiceSurnameTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.InvoiceSurnameTextbox.Location = new System.Drawing.Point(89, 55);
            this.InvoiceSurnameTextbox.Name = "InvoiceSurnameTextbox";
            this.InvoiceSurnameTextbox.Size = new System.Drawing.Size(150, 20);
            this.InvoiceSurnameTextbox.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(42, 138);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Miasto";
            // 
            // InvoiceCityTextbox
            // 
            this.InvoiceCityTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.InvoiceCityTextbox.Location = new System.Drawing.Point(89, 135);
            this.InvoiceCityTextbox.Name = "InvoiceCityTextbox";
            this.InvoiceCityTextbox.Size = new System.Drawing.Size(150, 20);
            this.InvoiceCityTextbox.TabIndex = 10;
            // 
            // InvoiceCopyCustDataCheck
            // 
            this.InvoiceCopyCustDataCheck.AutoSize = true;
            this.InvoiceCopyCustDataCheck.Location = new System.Drawing.Point(51, 170);
            this.InvoiceCopyCustDataCheck.Name = "InvoiceCopyCustDataCheck";
            this.InvoiceCopyCustDataCheck.Size = new System.Drawing.Size(185, 17);
            this.InvoiceCopyCustDataCheck.TabIndex = 11;
            this.InvoiceCopyCustDataCheck.Text = "Takie same jak dane kontrahenta";
            this.InvoiceCopyCustDataCheck.UseVisualStyleBackColor = true;
            this.InvoiceCopyCustDataCheck.CheckStateChanged += new System.EventHandler(this.InvoiceCopyCustDataCheck_CheckStateChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Kod pocztowy";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(48, 86);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Adres";
            // 
            // InvoiceAddressTextbox
            // 
            this.InvoiceAddressTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.InvoiceAddressTextbox.Location = new System.Drawing.Point(89, 83);
            this.InvoiceAddressTextbox.Name = "InvoiceAddressTextbox";
            this.InvoiceAddressTextbox.Size = new System.Drawing.Size(150, 20);
            this.InvoiceAddressTextbox.TabIndex = 8;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(54, 29);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(26, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Imię";
            // 
            // InvoiceNameTextbox
            // 
            this.InvoiceNameTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.InvoiceNameTextbox.Location = new System.Drawing.Point(89, 27);
            this.InvoiceNameTextbox.Name = "InvoiceNameTextbox";
            this.InvoiceNameTextbox.Size = new System.Drawing.Size(150, 20);
            this.InvoiceNameTextbox.TabIndex = 6;
            // 
            // itemsGroupbox
            // 
            this.itemsGroupbox.Controls.Add(this.ItemQuantityTextbox);
            this.itemsGroupbox.Controls.Add(this.label6);
            this.itemsGroupbox.Controls.Add(this.ItemIDTextbox);
            this.itemsGroupbox.Controls.Add(this.DeleteItemButton);
            this.itemsGroupbox.Controls.Add(this.AddItemButton);
            this.itemsGroupbox.Controls.Add(this.label31);
            this.itemsGroupbox.Controls.Add(this.OrderedItemsListbox);
            this.itemsGroupbox.Location = new System.Drawing.Point(24, 325);
            this.itemsGroupbox.Name = "itemsGroupbox";
            this.itemsGroupbox.Size = new System.Drawing.Size(556, 185);
            this.itemsGroupbox.TabIndex = 6;
            this.itemsGroupbox.TabStop = false;
            this.itemsGroupbox.Text = "Pozycje zamówienia";
            // 
            // ItemQuantityTextbox
            // 
            this.ItemQuantityTextbox.Location = new System.Drawing.Point(134, 150);
            this.ItemQuantityTextbox.Mask = "0000";
            this.ItemQuantityTextbox.Name = "ItemQuantityTextbox";
            this.ItemQuantityTextbox.PromptChar = ' ';
            this.ItemQuantityTextbox.Size = new System.Drawing.Size(100, 20);
            this.ItemQuantityTextbox.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(96, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Ilość";
            // 
            // ItemIDTextbox
            // 
            this.ItemIDTextbox.Location = new System.Drawing.Point(134, 124);
            this.ItemIDTextbox.Name = "ItemIDTextbox";
            this.ItemIDTextbox.PromptChar = ' ';
            this.ItemIDTextbox.Size = new System.Drawing.Size(100, 20);
            this.ItemIDTextbox.TabIndex = 20;
            // 
            // DeleteItemButton
            // 
            this.DeleteItemButton.Location = new System.Drawing.Point(412, 140);
            this.DeleteItemButton.Name = "DeleteItemButton";
            this.DeleteItemButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteItemButton.TabIndex = 22;
            this.DeleteItemButton.Text = "Usuń";
            this.DeleteItemButton.UseVisualStyleBackColor = true;
            this.DeleteItemButton.Click += new System.EventHandler(this.DeleteItemButton_Click);
            // 
            // AddItemButton
            // 
            this.AddItemButton.Location = new System.Drawing.Point(299, 140);
            this.AddItemButton.Name = "AddItemButton";
            this.AddItemButton.Size = new System.Drawing.Size(75, 23);
            this.AddItemButton.TabIndex = 21;
            this.AddItemButton.Text = "Dodaj";
            this.AddItemButton.UseVisualStyleBackColor = true;
            this.AddItemButton.Click += new System.EventHandler(this.AddItemButton_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(64, 127);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "ID Produktu";
            // 
            // OrderedItemsListbox
            // 
            this.OrderedItemsListbox.FormattingEnabled = true;
            this.OrderedItemsListbox.Location = new System.Drawing.Point(18, 23);
            this.OrderedItemsListbox.Name = "OrderedItemsListbox";
            this.OrderedItemsListbox.Size = new System.Drawing.Size(506, 95);
            this.OrderedItemsListbox.TabIndex = 0;
            // 
            // ProceedButton
            // 
            this.ProceedButton.Location = new System.Drawing.Point(717, 416);
            this.ProceedButton.Name = "ProceedButton";
            this.ProceedButton.Size = new System.Drawing.Size(75, 23);
            this.ProceedButton.TabIndex = 24;
            this.ProceedButton.Text = "Zatwierdź";
            this.ProceedButton.UseVisualStyleBackColor = true;
            this.ProceedButton.Click += new System.EventHandler(this.ProceedButton_Click);
            // 
            // CancelForm
            // 
            this.CancelForm.Location = new System.Drawing.Point(608, 416);
            this.CancelForm.Name = "CancelForm";
            this.CancelForm.Size = new System.Drawing.Size(75, 23);
            this.CancelForm.TabIndex = 23;
            this.CancelForm.Text = "Anuluj";
            this.CancelForm.UseVisualStyleBackColor = true;
            this.CancelForm.Click += new System.EventHandler(this.CancelForm_Click);
            // 
            // DeliveryNameTextbox
            // 
            this.DeliveryNameTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.DeliveryNameTextbox.Location = new System.Drawing.Point(90, 30);
            this.DeliveryNameTextbox.Name = "DeliveryNameTextbox";
            this.DeliveryNameTextbox.Size = new System.Drawing.Size(150, 20);
            this.DeliveryNameTextbox.TabIndex = 12;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(55, 32);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(26, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "Imię";
            // 
            // DeliveryAddressTextbox
            // 
            this.DeliveryAddressTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.DeliveryAddressTextbox.Location = new System.Drawing.Point(90, 87);
            this.DeliveryAddressTextbox.Name = "DeliveryAddressTextbox";
            this.DeliveryAddressTextbox.Size = new System.Drawing.Size(150, 20);
            this.DeliveryAddressTextbox.TabIndex = 14;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(49, 90);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 13);
            this.label24.TabIndex = 9;
            this.label24.Text = "Adres";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(9, 116);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 13);
            this.label23.TabIndex = 11;
            this.label23.Text = "Kod pocztowy";
            // 
            // DeliveryCityTextbox
            // 
            this.DeliveryCityTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.DeliveryCityTextbox.Location = new System.Drawing.Point(90, 139);
            this.DeliveryCityTextbox.Name = "DeliveryCityTextbox";
            this.DeliveryCityTextbox.Size = new System.Drawing.Size(150, 20);
            this.DeliveryCityTextbox.TabIndex = 16;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(43, 142);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(38, 13);
            this.label22.TabIndex = 13;
            this.label22.Text = "Miasto";
            // 
            // DeliverySurnameTextbox
            // 
            this.DeliverySurnameTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.DeliverySurnameTextbox.Location = new System.Drawing.Point(90, 58);
            this.DeliverySurnameTextbox.Name = "DeliverySurnameTextbox";
            this.DeliverySurnameTextbox.Size = new System.Drawing.Size(150, 20);
            this.DeliverySurnameTextbox.TabIndex = 13;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 61);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "Nazwisko";
            // 
            // DeliveryCopyCustDataCheck
            // 
            this.DeliveryCopyCustDataCheck.AutoSize = true;
            this.DeliveryCopyCustDataCheck.Location = new System.Drawing.Point(46, 231);
            this.DeliveryCopyCustDataCheck.Name = "DeliveryCopyCustDataCheck";
            this.DeliveryCopyCustDataCheck.Size = new System.Drawing.Size(185, 17);
            this.DeliveryCopyCustDataCheck.TabIndex = 19;
            this.DeliveryCopyCustDataCheck.Text = "Takie same jak dane kontrahenta";
            this.DeliveryCopyCustDataCheck.UseVisualStyleBackColor = true;
            this.DeliveryCopyCustDataCheck.CheckStateChanged += new System.EventHandler(this.DeliveryCopyCustDataCheck_CheckStateChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(22, 168);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(90, 13);
            this.label29.TabIndex = 19;
            this.label29.Text = "Sposób płatności";
            // 
            // PaymentMethodCombobox
            // 
            this.PaymentMethodCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PaymentMethodCombobox.Enabled = false;
            this.PaymentMethodCombobox.FormattingEnabled = true;
            this.PaymentMethodCombobox.Items.AddRange(new object[] {
            "Gotówka",
            "Karta płatnicza",
            "Przelew (14 dni)",
            "Płatność On-line"});
            this.PaymentMethodCombobox.Location = new System.Drawing.Point(118, 165);
            this.PaymentMethodCombobox.Name = "PaymentMethodCombobox";
            this.PaymentMethodCombobox.Size = new System.Drawing.Size(121, 21);
            this.PaymentMethodCombobox.TabIndex = 17;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(27, 196);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(85, 13);
            this.label30.TabIndex = 21;
            this.label30.Text = "Sposób dostawy";
            // 
            // DeliveryMethodCombobox
            // 
            this.DeliveryMethodCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DeliveryMethodCombobox.Enabled = false;
            this.DeliveryMethodCombobox.FormattingEnabled = true;
            this.DeliveryMethodCombobox.Items.AddRange(new object[] {
            "Odbiór osobisty",
            "Kurier",
            "Paczka pocztow",
            "Paczkomat"});
            this.DeliveryMethodCombobox.Location = new System.Drawing.Point(118, 193);
            this.DeliveryMethodCombobox.Name = "DeliveryMethodCombobox";
            this.DeliveryMethodCombobox.Size = new System.Drawing.Size(121, 21);
            this.DeliveryMethodCombobox.TabIndex = 18;
            // 
            // deliveryGroupbox
            // 
            this.deliveryGroupbox.Controls.Add(this.DeliveryPostcodeTextbox);
            this.deliveryGroupbox.Controls.Add(this.DeliveryMethodCombobox);
            this.deliveryGroupbox.Controls.Add(this.label30);
            this.deliveryGroupbox.Controls.Add(this.PaymentMethodCombobox);
            this.deliveryGroupbox.Controls.Add(this.label29);
            this.deliveryGroupbox.Controls.Add(this.label20);
            this.deliveryGroupbox.Controls.Add(this.DeliverySurnameTextbox);
            this.deliveryGroupbox.Controls.Add(this.label22);
            this.deliveryGroupbox.Controls.Add(this.DeliveryCityTextbox);
            this.deliveryGroupbox.Controls.Add(this.label23);
            this.deliveryGroupbox.Controls.Add(this.DeliveryCopyCustDataCheck);
            this.deliveryGroupbox.Controls.Add(this.label24);
            this.deliveryGroupbox.Controls.Add(this.DeliveryAddressTextbox);
            this.deliveryGroupbox.Controls.Add(this.label27);
            this.deliveryGroupbox.Controls.Add(this.DeliveryNameTextbox);
            this.deliveryGroupbox.Location = new System.Drawing.Point(614, 104);
            this.deliveryGroupbox.Name = "deliveryGroupbox";
            this.deliveryGroupbox.Size = new System.Drawing.Size(268, 265);
            this.deliveryGroupbox.TabIndex = 4;
            this.deliveryGroupbox.TabStop = false;
            this.deliveryGroupbox.Text = "Dane do wysyłki";
            // 
            // DeliveryPostcodeTextbox
            // 
            this.DeliveryPostcodeTextbox.BackColor = System.Drawing.Color.Moccasin;
            this.DeliveryPostcodeTextbox.Location = new System.Drawing.Point(90, 113);
            this.DeliveryPostcodeTextbox.Mask = "00-000";
            this.DeliveryPostcodeTextbox.Name = "DeliveryPostcodeTextbox";
            this.DeliveryPostcodeTextbox.PromptChar = ' ';
            this.DeliveryPostcodeTextbox.ResetOnSpace = false;
            this.DeliveryPostcodeTextbox.Size = new System.Drawing.Size(150, 20);
            this.DeliveryPostcodeTextbox.TabIndex = 15;
            this.DeliveryPostcodeTextbox.Leave += new System.EventHandler(this.DeliveryPostcodeTextbox_Leave);
            // 
            // PrintRecieptsButton
            // 
            this.PrintRecieptsButton.Location = new System.Drawing.Point(761, 520);
            this.PrintRecieptsButton.Name = "PrintRecieptsButton";
            this.PrintRecieptsButton.Size = new System.Drawing.Size(121, 23);
            this.PrintRecieptsButton.TabIndex = 25;
            this.PrintRecieptsButton.Text = "Wydruk paragonów";
            this.PrintRecieptsButton.UseVisualStyleBackColor = true;
            this.PrintRecieptsButton.Click += new System.EventHandler(this.PrintRecieptsButton_Click);
            // 
            // NewOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 555);
            this.Controls.Add(this.PrintRecieptsButton);
            this.Controls.Add(this.CancelForm);
            this.Controls.Add(this.ProceedButton);
            this.Controls.Add(this.itemsGroupbox);
            this.Controls.Add(this.invoiceGroupbox);
            this.Controls.Add(this.deliveryGroupbox);
            this.Controls.Add(this.customerGroupbox);
            this.Controls.Add(this.orderGroupbox);
            this.MaximizeBox = false;
            this.Name = "NewOrder";
            this.Text = "Nowe Zamówienie";
            this.orderGroupbox.ResumeLayout(false);
            this.orderGroupbox.PerformLayout();
            this.customerGroupbox.ResumeLayout(false);
            this.customerGroupbox.PerformLayout();
            this.invoiceGroupbox.ResumeLayout(false);
            this.invoiceGroupbox.PerformLayout();
            this.itemsGroupbox.ResumeLayout(false);
            this.itemsGroupbox.PerformLayout();
            this.deliveryGroupbox.ResumeLayout(false);
            this.deliveryGroupbox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox orderGroupbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox OrderNumberTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox OrderIDTextbox;
        private System.Windows.Forms.GroupBox customerGroupbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CustomerNameTextbox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox CustomerCityTextbox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox CustomerAddressTextbox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox CustomerSurnameTextbox;
        private System.Windows.Forms.GroupBox invoiceGroupbox;
        private System.Windows.Forms.CheckBox InvoiceCopyCustDataCheck;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox InvoiceSurnameTextbox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox InvoiceCityTextbox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox InvoiceAddressTextbox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox InvoiceNameTextbox;
        private System.Windows.Forms.GroupBox itemsGroupbox;
        private System.Windows.Forms.Button DeleteItemButton;
        private System.Windows.Forms.Button AddItemButton;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ListBox OrderedItemsListbox;
        private System.Windows.Forms.Button ProceedButton;
        private System.Windows.Forms.Button CancelForm;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.MaskedTextBox CustomerPostcodeTextbox;
        private System.Windows.Forms.MaskedTextBox InvoicePostcodeTextbox;
        private System.Windows.Forms.TextBox DeliveryNameTextbox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox DeliveryAddressTextbox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox DeliveryCityTextbox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox DeliverySurnameTextbox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox DeliveryCopyCustDataCheck;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox PaymentMethodCombobox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox DeliveryMethodCombobox;
        private System.Windows.Forms.GroupBox deliveryGroupbox;
        private System.Windows.Forms.MaskedTextBox DeliveryPostcodeTextbox;
        private System.Windows.Forms.MaskedTextBox ItemIDTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CustomerEmailTextbox;
        private System.Windows.Forms.MaskedTextBox ItemQuantityTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button PrintRecieptsButton;
    }
}


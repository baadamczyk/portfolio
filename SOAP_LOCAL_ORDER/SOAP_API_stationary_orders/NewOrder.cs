﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SOAP_API_stationary_orders.webapi;
using System.Net;

namespace SOAP_API_stationary_orders
{
    public partial class NewOrder : Form
    {
        //public HttpSessionState SessionState { get; set; }
        eCommerce24hApi service;

        Order newOrder = new Order();
        Customer newCustomer = new Customer();
        Invoice newInvoice = new Invoice();
        Delivery newDelivery = new Delivery();
        public int NewOrderID;
        List<orderProducts> OrderedItems = new List<webapi.orderProducts>();
        orderFullInfo FullInfo = new webapi.orderFullInfo();
        List<string> ListboxContent = new List<string>();

        public NewOrder()
        {
            InitializeComponent();
            service = new eCommerce24hApi();
            service.CookieContainer = new CookieContainer(); //STĄD WIE, JAKIE MA CIASTECZKA! TUTAJ PRZECHOWUJE INFORMACJE O SESJI!!
            DeliveryMethodCombobox.SelectedIndex = 0;
            PaymentMethodCombobox.SelectedIndex = 0;

            //Logowanie
            try
            {
                loginData login = new webapi.loginData();
                login.login = Credentials.login;
                login.password = Credentials.password;
                bool islogin = service.doLogin(login);
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Brak połączenia z serwerem!\nSpróbuj ponownie później.");
                Application.Exit();
            }
        }

        /********************** OBSŁUGA ELEMENTÓW FORMULARZA ******************/

        //Obsługa przycisków
        private void CancelForm_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ProceedButton_Click(object sender, EventArgs e)
        {
            DialogResult EntryAccept = MessageBox.Show("Czy dane są poprawne?", "Nowe Zamówienie", MessageBoxButtons.YesNo);
            if (EntryAccept == DialogResult.Yes)
            {
                if (AreTextoboxesNull() == false)
                {
                    GetValuesFromTextboxes();
                    ChoosePaymentAndDelivery();
                    ProceedWithOrder();
                }
                else
                {
                    MessageBox.Show("WSZYSTKIE POLA MUSZĄ BYĆ WYPEŁNIONE!");
                    return;
                }
            }
            else { }
        }

        private void AddItemButton_Click(object sender, EventArgs e)
        {
            if (IsQuantityNull() == true) return;
            if (ProductExists() == false) return;
            AddItemToLocalList();
            ShowDataInListbox();
        }

        private void DeleteItemButton_Click(object sender, EventArgs e)
        {
            if (OrderedItems.Count > 0)
            {
                OrderedItems.Remove(OrderedItems[OrderedItemsListbox.SelectedIndex]);
                ShowDataInListbox();
            }
            else MessageBox.Show("NIE MA NIC DO USUNIĘCIA!");
        }

        //Obsługa Checkboxów        
        private void InvoiceCopyCustDataCheck_CheckStateChanged(object sender, EventArgs e)
        {
            newInvoice.SameAsCustomer = InvoiceCopyCustDataCheck.Checked;
        }

        private void DeliveryCopyCustDataCheck_CheckStateChanged(object sender, EventArgs e)
        {
            newDelivery.SameAsCustomer = DeliveryCopyCustDataCheck.Checked;
        }



        /********************** KONIEC OBSŁUGI ELEMENTÓW FORMULARZA ******************/

        void AddItemToLocalList()
        {
            orderProducts ListRecord = new webapi.orderProducts();

            ListRecord.product_code = ItemIDTextbox.Text.ToString();

            if (OrderedItems.Count == 0) //Trzeba utworzyć pierwszy element listy, żeby potem działać na reszcie
            {
                ListRecord.product_code = ItemIDTextbox.Text.ToString();
                ListRecord.quantity = decimal.Parse(ItemQuantityTextbox.Text.ToString());
                OrderedItems.Add(ListRecord);
            }
            else
            {
                foreach (orderProducts item in OrderedItems)
                {
                    if (item.product_code == ItemIDTextbox.Text.ToString())
                    {
                        item.quantity += decimal.Parse(ItemQuantityTextbox.Text.ToString());
                        break; //bez tego wyrzuci, że nie może przeszkuać kolekcji
                    }
                    else
                    {
                        if (!IsProductOnList())
                        {
                            ListRecord.product_code = ItemIDTextbox.Text.ToString();
                            ListRecord.quantity = decimal.Parse(ItemQuantityTextbox.Text.ToString());
                            OrderedItems.Add(ListRecord);
                            break;
                        }
                    }
                }
            }
        }

        bool IsProductOnList()
        {
            foreach (orderProducts item in OrderedItems)
            {
                if (item.product_code == ItemIDTextbox.Text.ToString()) return true;
            }
            return false;
        }

        bool IsQuantityNull()
        {
            if (ItemQuantityTextbox.Text.ToString() == "")
            {
                MessageBox.Show("PROSZĘ WPROWADZIĆ ILOŚĆ PRODUKTU!");
                ItemQuantityTextbox.Text = "";
                ItemQuantityTextbox.Focus();
                return true;
            }
            return false;
        }

        bool AreTextoboxesNull()
        {
            if (CustomerEmailTextbox.Text == ""
             || CustomerNameTextbox.Text == ""
             || CustomerSurnameTextbox.Text == ""
             || CustomerPostcodeTextbox.Text == " -"
             || CustomerCityTextbox.Text == ""
             || CustomerAddressTextbox.Text == "") return true;

            if (newInvoice.SameAsCustomer == false)
            {
                if (InvoiceNameTextbox.Text == ""
                 || InvoiceSurnameTextbox.Text == ""
                 || InvoicePostcodeTextbox.Text == " -"
                 || InvoiceCityTextbox.Text == ""
                 || InvoiceAddressTextbox.Text == "") return true;
            }

            if (newDelivery.SameAsCustomer == false)
            {
                if (DeliveryNameTextbox.Text == ""
                 || DeliverySurnameTextbox.Text == ""
                 || DeliveryPostcodeTextbox.Text == " -"
                 || DeliveryCityTextbox.Text == ""
                 || DeliveryAddressTextbox.Text == "") return true;
            }
            return false;
        }


        bool ProductExists()
        {
            orderProducts verifyProduct = new webapi.orderProducts();
            verifyProduct.product_code = ItemIDTextbox.Text.ToString();
            try
            {
                service.getProduct(verifyProduct.product_code, true, false);
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Brak połączenia z serwerem!\nSpróbuj ponownie później.");
                return false;
            }
            catch(System.Web.Services.Protocols.SoapHeaderException)
            {
                MessageBox.Show("PRODUKT NIE ISTNIEJE!");
                ItemIDTextbox.Text = "";
                ItemIDTextbox.Focus();
                return false;
            }    
            return true;
        }

        void ShowDataInListbox()
        {
            List<string> ListboxRows = new List<string>();
            ListboxRows.Clear();

            foreach (orderProducts product in OrderedItems)
            {
                string ProductCodeString = product.product_code.ToString();
                string ProductNameString = service.getProduct(product.product_code, true, false).name;
                string ProductQuantityString = product.quantity.Value.ToString();
                string ProductInfoString = ProductCodeString + ": " + ProductNameString + " Qty: " + ProductQuantityString;
                ListboxRows.Add(ProductInfoString);
            }
            OrderedItemsListbox.DataSource = ListboxRows;
            ItemIDTextbox.Text = "";
            ItemIDTextbox.Focus();
        }

        //Pobranie danych z pól formularza
        void GetValuesFromTextboxes()
        {
            //Sczytanie danych klienta
            newCustomer.Name = CustomerNameTextbox.Text.ToString();
            newCustomer.LastName = CustomerSurnameTextbox.Text.ToString();
            newCustomer.Address = CustomerAddressTextbox.Text.ToString();
            newCustomer.Postcode = CustomerPostcodeTextbox.Text.ToString();
            newCustomer.City = CustomerCityTextbox.Text.ToString();
            newCustomer.Country = "Polska";
            newCustomer.Email = CustomerEmailTextbox.Text.ToString();

            //Sczytanie danych do faktury
            if (newInvoice.SameAsCustomer == true)
            {
                newInvoice.Name = newCustomer.Name;
                newInvoice.LastName = newCustomer.LastName;
                newInvoice.Address = newCustomer.Address;
                newInvoice.Postcode = newCustomer.Postcode;
                newInvoice.City = newCustomer.City;
                newInvoice.Country = newCustomer.Country;
            }
            else
            {
                newInvoice.Name = InvoiceNameTextbox.Text.ToString();
                newInvoice.LastName = InvoiceSurnameTextbox.Text.ToString();
                newInvoice.Address = InvoiceAddressTextbox.Text.ToString();
                newInvoice.Postcode = InvoicePostcodeTextbox.Text.ToString();
                newInvoice.City = InvoiceCityTextbox.Text.ToString();
                newInvoice.Country = "Polska";
            }

            //Sczytanie danych do wysyłki
            if (newDelivery.SameAsCustomer == true)
            {
                newDelivery.Name = newCustomer.Name;
                newDelivery.LastName = newCustomer.LastName;
                newDelivery.Address = newCustomer.Address;
                newDelivery.Postcode = newCustomer.Postcode;
                newDelivery.City = newCustomer.City;
                newDelivery.Country = newCustomer.Country;
            }
            else
            {
                newDelivery.Name = DeliveryNameTextbox.Text.ToString();
                newDelivery.LastName = DeliverySurnameTextbox.Text.ToString();
                newDelivery.Address = DeliveryAddressTextbox.Text.ToString();
                newDelivery.Postcode = DeliveryPostcodeTextbox.Text.ToString();
                newDelivery.City = DeliveryCityTextbox.Text.ToString();
                newDelivery.Country = "Polska";
            }
        }

        void ChoosePaymentAndDelivery()
        {
            //Sposób dostawy & sposób płatności
            switch (PaymentMethodCombobox.SelectedIndex)
            {
                case 0:
                    newDelivery.DeliveryMethod = "free_free";
                    break;
                case 1:
                    newDelivery.DeliveryMethod = "_kurier";
                    break;
                case 2:
                    newDelivery.DeliveryMethod = "_pocztapolska";
                    break;
                case 3:
                    newDelivery.DeliveryMethod = "_paczkomat";
                    break;
                default: break;
            }

            switch (DeliveryMethodCombobox.SelectedIndex)
            {
                case 0:
                    newDelivery.PaymentMethod = "platneprzyodbiorze";
                    break;
                case 1:
                    newDelivery.PaymentMethod = "_kartaplatnicza";
                    break;
                case 2:
                    newDelivery.PaymentMethod = "_przelewbankowy";
                    break;
                case 3:
                    newDelivery.PaymentMethod = "_platnosconline";
                    break;
                default: break;
            }
        }

        void MergeData()
        {
            customerAddress CAddress = new webapi.customerAddress();
            customerAddress InvAddress = new webapi.customerAddress();
            customerAddress DelAddress = new webapi.customerAddress();

            CAddress.firstname = newCustomer.Name;
            CAddress.lastname = newCustomer.LastName;
            CAddress.street_address = newCustomer.Address;
            CAddress.postcode = newCustomer.Postcode;
            CAddress.city = newCustomer.City;
            CAddress.country = newCustomer.Country;
            CAddress.email_address = newCustomer.Email;

            DelAddress.firstname = newDelivery.Name;
            DelAddress.lastname = newDelivery.LastName;
            DelAddress.street_address = newDelivery.Address;
            DelAddress.postcode = newDelivery.Postcode;
            DelAddress.city = newDelivery.City;
            DelAddress.country = newDelivery.Country;

            InvAddress.firstname = newInvoice.Name;
            InvAddress.lastname = newInvoice.LastName;
            InvAddress.street_address = newInvoice.Address;
            InvAddress.postcode = newInvoice.Postcode;
            InvAddress.city = newInvoice.City;
            InvAddress.country = newDelivery.Country;

            FullInfo.payment_method = "platneprzyodbiorze";
            FullInfo.transport_method = "free_free";
            FullInfo.customer_address = CAddress;
            FullInfo.delivery_address = DelAddress;
            FullInfo.billing_address = InvAddress;
            FullInfo.orderProducts = OrderedItems.ToArray();
        }

        void SendOrder()
        {
            try
            {
                NewOrderID = service.setOrder(FullInfo);
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Brak połączenia z serwerem!\nSpróbuj ponownie później.");
                return;
            }
            catch (System.Web.Services.Protocols.SoapHeaderException)
            {
                MessageBox.Show("Koszyk jest pusty! Dodaj produkty!");
                return;
            }
            catch (System.InvalidOperationException)
            {
                MessageBox.Show("NIE MOŻNA DODAĆ ZAMÓWIENIA! SPRAWDŹ STANY MAGAZYNOWE!");
                return;
            }
            MessageBox.Show("Zamówienie o numerze " + NewOrderID.ToString() + " dodane poprawnie!");
            IsRecieptRequired();
        }

        void ShowOrderIDOnForm()
        {
            OrderIDTextbox.Text = NewOrderID.ToString();
        }

        void ProceedWithOrder()
        {
            MergeData();
            SendOrder();
            ShowOrderIDOnForm();
        }
        bool IsPostcodeValid()
        {
            if (CustomerPostcodeTextbox.Text.Length != 6 || CustomerPostcodeTextbox.Text.Contains(" ")) return false;
            else return true;
        }

        //Walidacja pół kodu kreskowego i adresu e-mail (maska nie przepuszcza znaków specjalnych)
        private void CustomerPostcodeTextbox_Leave(object sender, EventArgs e)
        {

            if (!IsPostcodeValid())
            {
                CustomerPostcodeTextbox.Text = "";
                CustomerPostcodeTextbox.Focus();
                MessageBox.Show("Kod kreskowy jest niepoprawny!\nPopraw wpisaną wartość!");
            }
        }

        private void InvoicePostcodeTextbox_Leave(object sender, EventArgs e)
        {
            if (!IsPostcodeValid())
            {
                InvoicePostcodeTextbox.Text = "";
                InvoicePostcodeTextbox.Focus();
                MessageBox.Show("Kod kreskowy jest niepoprawny!\nPopraw wpisaną wartość!");
            }
        }

        private void DeliveryPostcodeTextbox_Leave(object sender, EventArgs e)
        {
            if (!IsPostcodeValid())
            {
                DeliveryPostcodeTextbox.Text = "";
                DeliveryPostcodeTextbox.Focus();
                MessageBox.Show("Kod kreskowy jest niepoprawny!\nPopraw wpisaną wartość!");
            }
        }

        private void CustomerEmailTextbox_Leave(object sender, EventArgs e)
        {
            if (!CustomerEmailTextbox.Text.ToString().Contains('@'))
            {
                CustomerEmailTextbox.Text = "";
                CustomerEmailTextbox.Focus();
                MessageBox.Show("Adres e-mail musi zawierać znak '@' !\nPopraw wpisaną wartość!");
            }
        }

        void IsRecieptRequired()
        {
            DialogResult RecieptChoice = new DialogResult();
            RecieptChoice = MessageBox.Show("Wydrukować paragon do zamówienia?", "Zamówienie",MessageBoxButtons.YesNo);

            if (RecieptChoice == DialogResult.Yes)
            {
                PrintOrder order = new PrintOrder();
                order.DoPrint(NewOrderID);
            }
            else return;
        }

        private void PrintRecieptsButton_Click(object sender, EventArgs e)
        {
            RecieptsPrinitng RecieptsPrinting = new RecieptsPrinitng();
            RecieptsPrinting.Show();
            this.Hide();
        }
    }
}
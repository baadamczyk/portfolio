﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOAP_API_stationary_orders
{
    class Order
    {
        public List<string> Basket = new List<string>();
        public int OrderID { get; set; }
        public string FullID { get; set; }
    }
}

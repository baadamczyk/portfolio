﻿using System.Collections.Generic;
using System.Windows.Forms;
using SOAP_API_stationary_orders.webapi;
using System.Net;
using AxTHERMALLIBLib;


namespace SOAP_API_stationary_orders
{
    class PrintOrder
    {
        eCommerce24hApi service;
        AxThermalLib Printer = new AxThermalLib();
        List<orderProducts> OrderedItems = new List<webapi.orderProducts>();
        productsFromOrder[] OrderedItemsArray;
        orderFullInfo FullInfo = new webapi.orderFullInfo();

        public PrintOrder()
        {
            service = new eCommerce24hApi();
            service.CookieContainer = new CookieContainer();

            try
            {
                loginData login = new webapi.loginData();
                login.login = Credentials.login;
                login.password = Credentials.password;
                bool islogin = service.doLogin(login);
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Brak połączenia z serwerem!\nSpróbuj ponownie później.");
                Application.Exit();
            }           
        }

        public void DoPrint(int OrderID)
        {
            GetOrderDetails(OrderID);
            PrintingInProgress PrintingAction = new PrintingInProgress();
            PrintingAction.DoPrint(OrderedItemsArray);
        }       

        public void GetOrderDetails(int OrderID)
        {
            try
            {
                service.getOrder(OrderID);
                OrderedItemsArray = service.getProductsFromOrder(OrderID);
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Brak połączenia z serwerem!\nSpróbuj ponownie później.");
                return;
            }
        }       
    }
}

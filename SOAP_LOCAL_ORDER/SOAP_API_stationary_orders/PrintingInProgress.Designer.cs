﻿namespace SOAP_API_stationary_orders
{
    partial class PrintingInProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintingInProgress));
            this.axThermalLib1 = new AxTHERMALLIBLib.AxThermalLib();
            ((System.ComponentModel.ISupportInitialize)(this.axThermalLib1)).BeginInit();
            this.SuspendLayout();
            // 
            // axThermalLib1
            // 
            this.axThermalLib1.Enabled = true;
            this.axThermalLib1.Location = new System.Drawing.Point(8, 4);
            this.axThermalLib1.Name = "axThermalLib1";
            this.axThermalLib1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axThermalLib1.OcxState")));
            this.axThermalLib1.Size = new System.Drawing.Size(108, 102);
            this.axThermalLib1.TabIndex = 0;
            // 
            // PrintingInProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(120, 113);
            this.Controls.Add(this.axThermalLib1);
            this.Name = "PrintingInProgress";
            this.Text = "PrintingInProgress";
            ((System.ComponentModel.ISupportInitialize)(this.axThermalLib1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxTHERMALLIBLib.AxThermalLib axThermalLib1;
    }
}
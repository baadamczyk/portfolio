﻿using System;
using System.Windows.Forms;
using SOAP_API_stationary_orders.webapi;

namespace SOAP_API_stationary_orders
{
    public partial class PrintingInProgress : Form
    {
        string ProductDescription = "";
        string ProductQuantity;
        string ProductPrice = "";
        string PTUValue = "";
        double Discount = 0; //Rabat/zniżka [wartość bezwględna]

        double RecieptSum = 0;        

        public PrintingInProgress()
        {
            InitializeComponent();
            try
            {
                axThermalLib1.THLOpenPort("COM4");
            }
            catch
            {
                MessageBox.Show("Nie można połączyć z drukarką.\nAkcja została anulowana");
            }
        }        

        public void DoPrint(productsFromOrder[] InputArray)
        {           
            axThermalLib1.LBTRSHDR(0, 3, "", "", "");
            PrintRecieptLines(InputArray);
            EndReciept();                                  
        }     
        
        void PrintRecieptLines(productsFromOrder[] InputArray)
        {
            short LineCounter = 1;
            foreach (productsFromOrder record in InputArray)
            {
                short LineNumber = LineCounter++;
                double SubSum;
                ProductDescription = record.product_code;
                ProductQuantity = record.quantity;
                ProductPrice = record.price;
                string TempTax = record.tax;
                TempTax = TempTax.Replace('.', ',');
                PTUValue = ChoosePTU(double.Parse(TempTax));
                ProductQuantity = ProductQuantity.Replace('.', ',');
                ProductPrice = ProductPrice.Replace('.', ',');
                double TempQuantity = double.Parse(ProductQuantity);
                double TempPrice = Math.Round(double.Parse(ProductPrice), 2);
                SubSum = TempPrice * TempQuantity;
                RecieptSum += Math.Round(SubSum, 2);

                axThermalLib1.LBTRSLN(LineNumber, 0, 0, ProductDescription, TempQuantity.ToString(), PTUValue, TempPrice.ToString(), SubSum.ToString(), "", "");
            }
        }
        
        string ChoosePTU(double tax)
        {
            if (tax == 23) return "A";
            if (tax == 0) return "B";
            if (tax == 5) return "C";
            if (tax == 5) return "D";
            else return "G";                               
        }  
        
        void EndReciept()
        {
            RecieptSum -= Discount;
            axThermalLib1.LBTREXIT(0, 0, 0, "K01", "", "", "", RecieptSum.ToString(), RecieptSum.ToString(), Discount.ToString());
        } 
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SOAP_API_stationary_orders.webapi;

namespace SOAP_API_stationary_orders
{
    public partial class SignIn : Form
    {
        eCommerce24hApi service;
        public SignIn()
        {
            InitializeComponent();
            service = new eCommerce24hApi();
        }

        private void SignInButton_Click(object sender, EventArgs e)
        {
            //ResetCredentials();

            if (AreCredentialsSetToDefault())
            {
                Credentials.login = Properties.Settings.Default.Username;
                Credentials.password = Properties.Settings.Default.Password;
                LoginTextbox.Text = Credentials.login;
                PasswordTextbox.Text = Credentials.password;
            }
            else
            {
                Credentials.login = LoginTextbox.Text.ToString();
                Credentials.password = PasswordTextbox.Text.ToString();
            }
                        
                        
            loginData login = new webapi.loginData();
            login.login =Credentials.login;
            login.password = Credentials.password;

            try
            {
                if (service.doLogin(login))
                {
                    MessageBox.Show("Dane logowania poprawne!");
                    if (SaveLoginDataCheckbox.Checked == true) SaveDefaultSettings();
                    NewOrder NewOrder = new NewOrder();
                    this.Hide();
                    NewOrder.Show();
                }
                else
                {
                    MessageBox.Show("Dane logowania niepoprawne!\nSpróbuj ponownie.");
                    return;
                }
            }
            catch (System.Net.WebException)               
            {
                MessageBox.Show("Brak połączenia z serwerem!\nSpróbuj ponownie później.");
                return;          
            }    
        }
        
        void SaveDefaultSettings()
        {
            Properties.Settings.Default.Username = LoginTextbox.Text.ToString();
            Properties.Settings.Default.Password = PasswordTextbox.Text.ToString();
            Properties.Settings.Default.Save();
        }      
        
        bool AreCredentialsSetToDefault()
        {
            if (Properties.Settings.Default.Username == "" && Properties.Settings.Default.Password == "") return false;
            else return true;
        }

        void ResetCredentials()
        {
            Properties.Settings.Default.Username = "";
            Properties.Settings.Default.Password = "";
            Properties.Settings.Default.Save();
        }           
    }
}

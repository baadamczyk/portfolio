﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace calculator.tests
{
    [TestClass]    
    public class OperationTests
    {
        Operations Operations = new Operations();
        
        //ADD
        [TestMethod]
        public void Add_AddTwoPositiveValues_Calculated()
        {            
            Assert.AreEqual(4, Operations.Add(2, 2));
        }

        [TestMethod]
        public void Add_AddTwoNegativeValues_Calculated()
        {
            Assert.AreEqual(-4, Operations.Add(-2, -2));
        }

        [TestMethod]
        public void Add_AddPositiveAndNegativeValues_Calculated()
        {
            Assert.AreEqual(2, Operations.Add(5, -3));
        }
        
        //SUBSTRACT
        [TestMethod]
        public void Substract_SubstractTwoPositiveValues_Calculated()
        {
            Assert.AreEqual(10, Operations.Substract(15, 5));
        }

        [TestMethod]
        public void Substract_SubstractTwoNegativeValues_Calculated()
        {
            Assert.AreEqual(-1, Operations.Substract(-5, -4));
        }

        [TestMethod]
        public void Substract_SubstractPositiveAndNegativeValues_Calculated()
        {
            Assert.AreEqual(9, Operations.Substract(5, -4));
        }

        [TestMethod]
        public void Substract_SubstractNegativeAndPositiveValues_Calculated()
        {
            Assert.AreEqual(-9, Operations.Substract(-5, 4));
        }

        //MULTIPLY
        [TestMethod]
        public void Multiply_MultiplyTwoPositiveValues_Calculated()
        {
            Assert.AreEqual(4, Operations.Multiply(2, 2));
        }

        [TestMethod]
        public void Multiply_MultiplyTwoNegatveValues_Calculated()
        {
            Assert.AreEqual(4, Operations.Multiply(-2, -2));
        }

        [TestMethod]
        public void Multiply_MultiplyPositiveAndNegativeValues_Calculated()
        {
            Assert.AreEqual(-4, Operations.Multiply(2, -2));
        }

        [TestMethod]
        public void Multiply_MultiplyByZero_Calculated()
        {
            Assert.AreEqual(0, Operations.Multiply(2, 0));
        }

        //DIVIDE
        [TestMethod]
        public void Divide_DivideTwoPositiveValues_Calculated()
        {
            Assert.AreEqual(2,Operations.Divide(4,2));
        }

        [TestMethod]
        public void Divide_DivideTwoNegativeValues_Calculated()
        {
            Assert.AreEqual(2, Operations.Divide(-4, -2));
        }

        [TestMethod]
        public void Divide_DividePositiveAndNegativeValues_Calculated()
        {
            Assert.AreEqual(-2, Operations.Divide(4, -2));
        }

        [TestMethod]
        public void Divide_DivideNegativeAndPositiveValues_Calculated()
        {
            Assert.AreEqual(-2, Operations.Divide(-4, 2));
        }

        [TestMethod]
        public void Divide_DivideZeroByValue_Calculated()
        {
            Assert.AreEqual(0, Operations.Divide(0, -2));
        }

        [TestMethod]        
        public void Divide_DiviveByZero_ThrowsException()
        {
            Assert.AreEqual(0,Operations.Divide(2, 0));
        }        
    }
    
}

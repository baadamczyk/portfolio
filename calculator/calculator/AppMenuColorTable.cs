﻿using System.Drawing;
using System.Windows.Forms;

namespace calculator
{
    public class AppMenuColorTable: ProfessionalColorTable
    {
        public override Color MenuItemSelected
        {
            get { return Color.DimGray ; }
        }

        public override Color MenuBorder
        {
            get { return Color.DimGray; }
        }

        public override Color MenuItemBorder
        {
            get { return Color.DimGray; }
        }

        public override Color MenuItemPressedGradientBegin
        {
            get { return Color.DimGray; }
        }

        public override Color MenuItemPressedGradientMiddle
        {
            get { return Color.DimGray; }
        }

        public override Color MenuItemPressedGradientEnd       
        {
            get { return Color.DimGray; }
        }

        public override Color MenuItemSelectedGradientBegin
        {
            get { return Color.DimGray; }
        }

        public override Color MenuItemSelectedGradientEnd
        {
            get { return Color.DimGray; }
        }
    }
}

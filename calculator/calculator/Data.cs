﻿using System;
using System.Linq;

namespace calculator
{
    public class Data
    {
        public string DataToDisplay { get; set; }
        string CalcBuffer;        
        static double XValue;
        static double YValue;
        static bool IsXValuePresent = false;
        static bool IsYValuePresent = false;
        static string OperationType;

        Operations Operations = new Operations();

        private void ConvertRawDataToDouble()
        {            
            if (!IsXValuePresent)
            {
                XValue = double.Parse(CalcBuffer);
                IsXValuePresent = true;                
            }
            else if (!IsYValuePresent)
            {
                YValue = double.Parse(CalcBuffer);
                IsYValuePresent = true;
            }            
        }

        private void SendDataToDisplay()
        {
            if (CalcBuffer.Length > 8) DataToDisplay = CalcBuffer.Remove(8);
            else DataToDisplay = CalcBuffer;
        }

        private void ClearCalcBuffer()
        {
            CalcBuffer = "";    
        }

        public void PreformActionOnData(int ActionID)
        {
            if (ActionID <= 9) AppendNumber(ActionID);
            else if (ActionID == 10) AppendComma();
            else if (ActionID == 11) EraseOneCharacter();
            else if (ActionID <= 15) CalculateByMethod(ActionID);
            else if (ActionID == 16) ClearCalculationData();
            else if (ActionID == 17) ConvertNumberToNegative();
            else if (ActionID == 18) ShowResult();
            else throw new InvalidActionIDException();            
        }

        private void AppendNumber(int actionID)
        {
            CalcBuffer += actionID.ToString();
            SendDataToDisplay();            
        }
                
        private void AppendComma()
        {
            if (!BufferIsEmpty() && !CommaAppeared())
            {
                CalcBuffer += ",";
                SendDataToDisplay();
            }
        }

        private bool BufferIsEmpty()
        {
            if (CalcBuffer == "") return true;
            else return false;
        }

        private bool CommaAppeared()
        {
            if (CalcBuffer != "" && CalcBuffer != null) return CheckForComma();
            else return true;           
        }

        private bool CheckForComma()
        {
            if (CalcBuffer.Contains(',')) return true;
            else return false;
        }

        private void EraseOneCharacter()
        {
            if (CalcBuffer.Length!=0)
            {
                CalcBuffer = CalcBuffer.Remove(CalcBuffer.Length - 1, 1);
                SendDataToDisplay(); 
            }
        }

        private void CalculateByMethod(int actionID)
        {
            switch(actionID)
            {
                case 12:
                    PerformAdd();
                    break;
                case 13:
                    PerfomSubstract();
                    break;
                case 14:
                    PerformMultipy();
                    break;
                case 15:
                    PerformDivide();
                    break;
                default:
                    break;                
            }            
        }

        private void PerformAdd()
        {            
            SetOperationType("Add");
            if (BufferIsEmpty()) return;
                        
            ConvertRawDataToDouble();
            DoAddAction();            
            SendDataToDisplay();
            ClearCalcBuffer();
        }

        private void DoAddAction()
        {
            if (IsXValuePresent && IsYValuePresent)
            {
                XValue = Operations.Add(XValue, YValue);
                IsYValuePresent = false;
                CalcBuffer = XValue.ToString();
            }
        }              

        private void PerfomSubstract()
        {
            SetOperationType("Substract");
            if (BufferIsEmpty()) return;

            ConvertRawDataToDouble();
            DoSubstractAction();
            SendDataToDisplay();
            ClearCalcBuffer();
        }

        private void DoSubstractAction()
        {
            if (IsXValuePresent && IsYValuePresent)
            {
                XValue = Operations.Substract(XValue, YValue);
                IsYValuePresent = false;
                CalcBuffer = XValue.ToString();
            }
        }

        private void PerformMultipy()
        {
            SetOperationType("Multiply");
            if (BufferIsEmpty()) return;

            ConvertRawDataToDouble();
            DoMultiplyAction();
            SendDataToDisplay();
            ClearCalcBuffer();
        }

        private void DoMultiplyAction()
        {
            if (IsXValuePresent && IsYValuePresent)
            {
                XValue = Operations.Multiply(XValue, YValue);
                IsYValuePresent = false;
                CalcBuffer = XValue.ToString();
            }
        }

        private void PerformDivide()
        {
            SetOperationType("Divide");
            if (BufferIsEmpty()) return;

            ConvertRawDataToDouble();
            DoDivideAction();
            SendDataToDisplay();
            ClearCalcBuffer();
        }

        private void DoDivideAction()
        {
            if (IsXValuePresent && IsYValuePresent)
            {
                XValue = Operations.Divide(XValue, YValue);
                IsYValuePresent = false;
                CalcBuffer = XValue.ToString();
            }
        }

        private void SetOperationType(string operationType)
        {
            OperationType = operationType;
        }

        private void ClearCalculationData()
        {
            CalcBuffer = "";
            DataToDisplay = "";
            IsXValuePresent = false;
            IsYValuePresent = false;            
        }

       
        private void ConvertNumberToNegative()
        {
            try
            {
                double TempBuffer = double.Parse(CalcBuffer);
                TempBuffer *= (-1);
                CalcBuffer = TempBuffer.ToString();
                SendDataToDisplay();
            }
            catch (FormatException)
            {
                return;
            }            
        }               

        private void ShowResult()
        {
            switch(OperationType)
            {
                case "Add":
                    PerformAdd();
                    break;
                case "Substract":
                    PerfomSubstract();
                    break;
                case "Multiply":
                    PerformMultipy();
                    break;
                case "Divide":
                    PerformDivide();
                    break;
                default:
                    break;
            }
        }       
    }
}

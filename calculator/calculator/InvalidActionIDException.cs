﻿using System;

namespace calculator
{
    public class InvalidActionIDException: Exception
    {
        public InvalidActionIDException() { }

        public InvalidActionIDException(string message)
            :base(message) { }

        public InvalidActionIDException(string message, Exception inner)
            :base(message, inner) { }
    }
}

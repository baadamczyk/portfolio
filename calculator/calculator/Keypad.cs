﻿namespace calculator
{
    public class Keypad: Keys
    { 
        Data Processor = new Data();
        public void InterpretKeyInput(int KeyID)
        {
            CalcKeys Key = (CalcKeys)KeyID;
            InvokeKeypressedAction(Key);                                 
        }

        void InvokeKeypressedAction(CalcKeys KeyID)
        {
            int ActionID = ((int)KeyID);
            Processor.PreformActionOnData(ActionID);                       
        }
    }
}

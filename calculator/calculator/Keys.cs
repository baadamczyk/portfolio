﻿namespace calculator
{
    public class Keys
    {
        public enum CalcKeys
        {
            Zero,
            One,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Comma,
            Erase,
            Add,
            Substract,
            Multiply,
            Divide,                    
            Reset,                      
            NegativeNumber,
            Restult
        }
    }
}

﻿namespace calculator
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AppMenu = new System.Windows.Forms.MenuStrip();
            this.ModeSelectionItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SimpleModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AdvancedModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.DELButton = new System.Windows.Forms.Button();
            this.CEButton = new System.Windows.Forms.Button();
            this.OperatorsPanel = new System.Windows.Forms.Panel();
            this.ResultButton = new System.Windows.Forms.Button();
            this.MultiplyButton = new System.Windows.Forms.Button();
            this.SubButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.DivButton = new System.Windows.Forms.Button();
            this.KeypadPanel = new System.Windows.Forms.Panel();
            this.ThreeButton = new System.Windows.Forms.Button();
            this.CommaButton = new System.Windows.Forms.Button();
            this.SevenButton = new System.Windows.Forms.Button();
            this.EightButton = new System.Windows.Forms.Button();
            this.NineButton = new System.Windows.Forms.Button();
            this.FourButton = new System.Windows.Forms.Button();
            this.FiveButton = new System.Windows.Forms.Button();
            this.SixButton = new System.Windows.Forms.Button();
            this.OneButton = new System.Windows.Forms.Button();
            this.TwoButton = new System.Windows.Forms.Button();
            this.ZeroButton = new System.Windows.Forms.Button();
            this.NegativeButton = new System.Windows.Forms.Button();
            this.DisplayTextbox = new System.Windows.Forms.TextBox();
            this.AppMenu.SuspendLayout();
            this.panel2.SuspendLayout();
            this.OperatorsPanel.SuspendLayout();
            this.KeypadPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // AppMenu
            // 
            this.AppMenu.BackColor = System.Drawing.Color.Gray;
            this.AppMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ModeSelectionItem});
            this.AppMenu.Location = new System.Drawing.Point(0, 0);
            this.AppMenu.Name = "AppMenu";
            this.AppMenu.Size = new System.Drawing.Size(216, 24);
            this.AppMenu.TabIndex = 29;
            this.AppMenu.Text = "menuStrip1";
            // 
            // ModeSelectionItem
            // 
            this.ModeSelectionItem.BackColor = System.Drawing.Color.Gray;
            this.ModeSelectionItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SimpleModeToolStripMenuItem,
            this.AdvancedModeToolStripMenuItem});
            this.ModeSelectionItem.ForeColor = System.Drawing.Color.White;
            this.ModeSelectionItem.Name = "ModeSelectionItem";
            this.ModeSelectionItem.Size = new System.Drawing.Size(74, 20);
            this.ModeSelectionItem.Text = "Tryb pracy";
            // 
            // SimpleModeToolStripMenuItem
            // 
            this.SimpleModeToolStripMenuItem.BackColor = System.Drawing.Color.Gray;
            this.SimpleModeToolStripMenuItem.Checked = true;
            this.SimpleModeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SimpleModeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.SimpleModeToolStripMenuItem.Name = "SimpleModeToolStripMenuItem";
            this.SimpleModeToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.SimpleModeToolStripMenuItem.Text = "Prosty";
            // 
            // AdvancedModeToolStripMenuItem
            // 
            this.AdvancedModeToolStripMenuItem.BackColor = System.Drawing.Color.Gray;
            this.AdvancedModeToolStripMenuItem.Enabled = false;
            this.AdvancedModeToolStripMenuItem.Name = "AdvancedModeToolStripMenuItem";
            this.AdvancedModeToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.AdvancedModeToolStripMenuItem.Text = "Naukowy";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Coral;
            this.panel2.Controls.Add(this.DELButton);
            this.panel2.Controls.Add(this.CEButton);
            this.panel2.Location = new System.Drawing.Point(68, 98);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(89, 48);
            this.panel2.TabIndex = 35;
            // 
            // DELButton
            // 
            this.DELButton.BackColor = System.Drawing.Color.Transparent;
            this.DELButton.FlatAppearance.BorderSize = 0;
            this.DELButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DELButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DELButton.ForeColor = System.Drawing.Color.White;
            this.DELButton.Location = new System.Drawing.Point(46, 3);
            this.DELButton.Name = "DELButton";
            this.DELButton.Size = new System.Drawing.Size(40, 40);
            this.DELButton.TabIndex = 21;
            this.DELButton.Text = "DEL";
            this.DELButton.UseVisualStyleBackColor = false;
            this.DELButton.Click += new System.EventHandler(this.DELButton_Click);
            // 
            // CEButton
            // 
            this.CEButton.BackColor = System.Drawing.Color.Transparent;
            this.CEButton.FlatAppearance.BorderSize = 0;
            this.CEButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CEButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CEButton.ForeColor = System.Drawing.Color.White;
            this.CEButton.Location = new System.Drawing.Point(0, 4);
            this.CEButton.Name = "CEButton";
            this.CEButton.Size = new System.Drawing.Size(40, 40);
            this.CEButton.TabIndex = 23;
            this.CEButton.Text = "CE";
            this.CEButton.UseVisualStyleBackColor = false;
            this.CEButton.Click += new System.EventHandler(this.CEButton_Click);
            // 
            // OperatorsPanel
            // 
            this.OperatorsPanel.BackColor = System.Drawing.Color.Coral;
            this.OperatorsPanel.Controls.Add(this.ResultButton);
            this.OperatorsPanel.Controls.Add(this.MultiplyButton);
            this.OperatorsPanel.Controls.Add(this.SubButton);
            this.OperatorsPanel.Controls.Add(this.AddButton);
            this.OperatorsPanel.Controls.Add(this.DivButton);
            this.OperatorsPanel.Location = new System.Drawing.Point(157, 98);
            this.OperatorsPanel.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.OperatorsPanel.Name = "OperatorsPanel";
            this.OperatorsPanel.Size = new System.Drawing.Size(50, 243);
            this.OperatorsPanel.TabIndex = 34;
            // 
            // ResultButton
            // 
            this.ResultButton.BackColor = System.Drawing.Color.Transparent;
            this.ResultButton.FlatAppearance.BorderSize = 0;
            this.ResultButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ResultButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ResultButton.ForeColor = System.Drawing.Color.White;
            this.ResultButton.Location = new System.Drawing.Point(3, 191);
            this.ResultButton.Name = "ResultButton";
            this.ResultButton.Size = new System.Drawing.Size(40, 40);
            this.ResultButton.TabIndex = 15;
            this.ResultButton.Text = "=";
            this.ResultButton.UseVisualStyleBackColor = false;
            this.ResultButton.Click += new System.EventHandler(this.ResultButton_Click);
            // 
            // MultiplyButton
            // 
            this.MultiplyButton.BackColor = System.Drawing.Color.Transparent;
            this.MultiplyButton.FlatAppearance.BorderSize = 0;
            this.MultiplyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MultiplyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MultiplyButton.ForeColor = System.Drawing.Color.White;
            this.MultiplyButton.Location = new System.Drawing.Point(3, 53);
            this.MultiplyButton.Name = "MultiplyButton";
            this.MultiplyButton.Size = new System.Drawing.Size(40, 40);
            this.MultiplyButton.TabIndex = 12;
            this.MultiplyButton.Text = "x";
            this.MultiplyButton.UseVisualStyleBackColor = false;
            this.MultiplyButton.Click += new System.EventHandler(this.MultiplyButton_Click);
            // 
            // SubButton
            // 
            this.SubButton.BackColor = System.Drawing.Color.Transparent;
            this.SubButton.FlatAppearance.BorderSize = 0;
            this.SubButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SubButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SubButton.ForeColor = System.Drawing.Color.White;
            this.SubButton.Location = new System.Drawing.Point(3, 99);
            this.SubButton.Name = "SubButton";
            this.SubButton.Size = new System.Drawing.Size(40, 40);
            this.SubButton.TabIndex = 13;
            this.SubButton.Text = "-";
            this.SubButton.UseVisualStyleBackColor = false;
            this.SubButton.Click += new System.EventHandler(this.SubButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.BackColor = System.Drawing.Color.Transparent;
            this.AddButton.FlatAppearance.BorderSize = 0;
            this.AddButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddButton.ForeColor = System.Drawing.Color.White;
            this.AddButton.Location = new System.Drawing.Point(3, 145);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(40, 40);
            this.AddButton.TabIndex = 14;
            this.AddButton.Text = "+";
            this.AddButton.UseVisualStyleBackColor = false;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // DivButton
            // 
            this.DivButton.BackColor = System.Drawing.Color.Transparent;
            this.DivButton.FlatAppearance.BorderSize = 0;
            this.DivButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DivButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DivButton.ForeColor = System.Drawing.Color.White;
            this.DivButton.Location = new System.Drawing.Point(3, 4);
            this.DivButton.Name = "DivButton";
            this.DivButton.Size = new System.Drawing.Size(40, 40);
            this.DivButton.TabIndex = 16;
            this.DivButton.Text = "÷";
            this.DivButton.UseVisualStyleBackColor = false;
            this.DivButton.Click += new System.EventHandler(this.DivButton_Click);
            // 
            // KeypadPanel
            // 
            this.KeypadPanel.BackColor = System.Drawing.Color.DimGray;
            this.KeypadPanel.Controls.Add(this.ThreeButton);
            this.KeypadPanel.Controls.Add(this.CommaButton);
            this.KeypadPanel.Controls.Add(this.SevenButton);
            this.KeypadPanel.Controls.Add(this.EightButton);
            this.KeypadPanel.Controls.Add(this.NineButton);
            this.KeypadPanel.Controls.Add(this.FourButton);
            this.KeypadPanel.Controls.Add(this.FiveButton);
            this.KeypadPanel.Controls.Add(this.SixButton);
            this.KeypadPanel.Controls.Add(this.OneButton);
            this.KeypadPanel.Controls.Add(this.TwoButton);
            this.KeypadPanel.Controls.Add(this.ZeroButton);
            this.KeypadPanel.Controls.Add(this.NegativeButton);
            this.KeypadPanel.Location = new System.Drawing.Point(12, 148);
            this.KeypadPanel.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.KeypadPanel.Name = "KeypadPanel";
            this.KeypadPanel.Size = new System.Drawing.Size(145, 193);
            this.KeypadPanel.TabIndex = 33;
            // 
            // ThreeButton
            // 
            this.ThreeButton.FlatAppearance.BorderSize = 0;
            this.ThreeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ThreeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ThreeButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ThreeButton.Location = new System.Drawing.Point(102, 96);
            this.ThreeButton.Name = "ThreeButton";
            this.ThreeButton.Size = new System.Drawing.Size(40, 40);
            this.ThreeButton.TabIndex = 12;
            this.ThreeButton.Text = "3";
            this.ThreeButton.UseVisualStyleBackColor = true;
            this.ThreeButton.Click += new System.EventHandler(this.ThreeButton_Click);
            // 
            // CommaButton
            // 
            this.CommaButton.FlatAppearance.BorderSize = 0;
            this.CommaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CommaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CommaButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CommaButton.Location = new System.Drawing.Point(102, 142);
            this.CommaButton.Name = "CommaButton";
            this.CommaButton.Size = new System.Drawing.Size(40, 40);
            this.CommaButton.TabIndex = 11;
            this.CommaButton.Text = ",";
            this.CommaButton.UseVisualStyleBackColor = true;
            this.CommaButton.Click += new System.EventHandler(this.CommaButton_Click);
            // 
            // SevenButton
            // 
            this.SevenButton.FlatAppearance.BorderSize = 0;
            this.SevenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SevenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SevenButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SevenButton.Location = new System.Drawing.Point(3, 4);
            this.SevenButton.Name = "SevenButton";
            this.SevenButton.Size = new System.Drawing.Size(40, 40);
            this.SevenButton.TabIndex = 0;
            this.SevenButton.Text = "7";
            this.SevenButton.UseVisualStyleBackColor = true;
            this.SevenButton.Click += new System.EventHandler(this.SevenButton_Click);
            // 
            // EightButton
            // 
            this.EightButton.FlatAppearance.BorderSize = 0;
            this.EightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EightButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.EightButton.Location = new System.Drawing.Point(56, 3);
            this.EightButton.Name = "EightButton";
            this.EightButton.Size = new System.Drawing.Size(40, 40);
            this.EightButton.TabIndex = 1;
            this.EightButton.Text = "8";
            this.EightButton.UseVisualStyleBackColor = true;
            this.EightButton.Click += new System.EventHandler(this.EightButton_Click);
            // 
            // NineButton
            // 
            this.NineButton.FlatAppearance.BorderSize = 0;
            this.NineButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NineButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NineButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.NineButton.Location = new System.Drawing.Point(102, 3);
            this.NineButton.Name = "NineButton";
            this.NineButton.Size = new System.Drawing.Size(40, 40);
            this.NineButton.TabIndex = 2;
            this.NineButton.Text = "9";
            this.NineButton.UseVisualStyleBackColor = true;
            this.NineButton.Click += new System.EventHandler(this.NineButton_Click);
            // 
            // FourButton
            // 
            this.FourButton.FlatAppearance.BorderSize = 0;
            this.FourButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FourButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FourButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.FourButton.Location = new System.Drawing.Point(3, 50);
            this.FourButton.Name = "FourButton";
            this.FourButton.Size = new System.Drawing.Size(40, 40);
            this.FourButton.TabIndex = 3;
            this.FourButton.Text = "4";
            this.FourButton.UseVisualStyleBackColor = true;
            this.FourButton.Click += new System.EventHandler(this.FourButton_Click);
            // 
            // FiveButton
            // 
            this.FiveButton.FlatAppearance.BorderSize = 0;
            this.FiveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FiveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FiveButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.FiveButton.Location = new System.Drawing.Point(56, 50);
            this.FiveButton.Name = "FiveButton";
            this.FiveButton.Size = new System.Drawing.Size(40, 40);
            this.FiveButton.TabIndex = 4;
            this.FiveButton.Text = "5";
            this.FiveButton.UseVisualStyleBackColor = true;
            this.FiveButton.Click += new System.EventHandler(this.FiveButton_Click);
            // 
            // SixButton
            // 
            this.SixButton.FlatAppearance.BorderSize = 0;
            this.SixButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SixButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SixButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SixButton.Location = new System.Drawing.Point(102, 50);
            this.SixButton.Name = "SixButton";
            this.SixButton.Size = new System.Drawing.Size(40, 40);
            this.SixButton.TabIndex = 5;
            this.SixButton.Text = "6";
            this.SixButton.UseVisualStyleBackColor = true;
            this.SixButton.Click += new System.EventHandler(this.SixButton_Click);
            // 
            // OneButton
            // 
            this.OneButton.FlatAppearance.BorderSize = 0;
            this.OneButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OneButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.OneButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.OneButton.Location = new System.Drawing.Point(3, 96);
            this.OneButton.Name = "OneButton";
            this.OneButton.Size = new System.Drawing.Size(40, 40);
            this.OneButton.TabIndex = 6;
            this.OneButton.Text = "1";
            this.OneButton.UseVisualStyleBackColor = true;
            this.OneButton.Click += new System.EventHandler(this.OneButton_Click);
            // 
            // TwoButton
            // 
            this.TwoButton.FlatAppearance.BorderSize = 0;
            this.TwoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TwoButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TwoButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TwoButton.Location = new System.Drawing.Point(56, 96);
            this.TwoButton.Name = "TwoButton";
            this.TwoButton.Size = new System.Drawing.Size(40, 40);
            this.TwoButton.TabIndex = 7;
            this.TwoButton.Text = "2";
            this.TwoButton.UseVisualStyleBackColor = true;
            this.TwoButton.Click += new System.EventHandler(this.TwoButton_Click);
            // 
            // ZeroButton
            // 
            this.ZeroButton.FlatAppearance.BorderSize = 0;
            this.ZeroButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ZeroButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ZeroButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ZeroButton.Location = new System.Drawing.Point(56, 142);
            this.ZeroButton.Name = "ZeroButton";
            this.ZeroButton.Size = new System.Drawing.Size(40, 40);
            this.ZeroButton.TabIndex = 9;
            this.ZeroButton.Text = "0";
            this.ZeroButton.UseVisualStyleBackColor = true;
            this.ZeroButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // NegativeButton
            // 
            this.NegativeButton.FlatAppearance.BorderSize = 0;
            this.NegativeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NegativeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NegativeButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.NegativeButton.Location = new System.Drawing.Point(3, 142);
            this.NegativeButton.Name = "NegativeButton";
            this.NegativeButton.Size = new System.Drawing.Size(40, 40);
            this.NegativeButton.TabIndex = 10;
            this.NegativeButton.Text = "±";
            this.NegativeButton.UseVisualStyleBackColor = true;
            this.NegativeButton.Click += new System.EventHandler(this.NegativeButton_Click);
            // 
            // DisplayTextbox
            // 
            this.DisplayTextbox.BackColor = System.Drawing.Color.Gray;
            this.DisplayTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DisplayTextbox.Font = new System.Drawing.Font("Tahoma", 33F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DisplayTextbox.ForeColor = System.Drawing.Color.White;
            this.DisplayTextbox.Location = new System.Drawing.Point(12, 38);
            this.DisplayTextbox.MaxLength = 8;
            this.DisplayTextbox.Name = "DisplayTextbox";
            this.DisplayTextbox.ReadOnly = true;
            this.DisplayTextbox.Size = new System.Drawing.Size(195, 54);
            this.DisplayTextbox.TabIndex = 38;
            this.DisplayTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(216, 352);
            this.Controls.Add(this.DisplayTextbox);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.OperatorsPanel);
            this.Controls.Add(this.KeypadPanel);
            this.Controls.Add(this.AppMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.AppMenu;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "CALC";
            this.Load += new System.EventHandler(this.Main_Load);
            this.AppMenu.ResumeLayout(false);
            this.AppMenu.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.OperatorsPanel.ResumeLayout(false);
            this.KeypadPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip AppMenu;
        private System.Windows.Forms.ToolStripMenuItem ModeSelectionItem;
        private System.Windows.Forms.ToolStripMenuItem SimpleModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AdvancedModeToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button DELButton;
        private System.Windows.Forms.Button CEButton;
        private System.Windows.Forms.Panel OperatorsPanel;
        private System.Windows.Forms.Button ResultButton;
        private System.Windows.Forms.Button MultiplyButton;
        private System.Windows.Forms.Button SubButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button DivButton;
        private System.Windows.Forms.Panel KeypadPanel;
        private System.Windows.Forms.Button ThreeButton;
        private System.Windows.Forms.Button CommaButton;
        private System.Windows.Forms.Button SevenButton;
        private System.Windows.Forms.Button EightButton;
        private System.Windows.Forms.Button NineButton;
        private System.Windows.Forms.Button FourButton;
        private System.Windows.Forms.Button FiveButton;
        private System.Windows.Forms.Button SixButton;
        private System.Windows.Forms.Button OneButton;
        private System.Windows.Forms.Button TwoButton;
        private System.Windows.Forms.Button ZeroButton;
        private System.Windows.Forms.Button NegativeButton;
        private System.Windows.Forms.TextBox DisplayTextbox;
    }
}


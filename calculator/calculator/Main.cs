﻿using System;
using System.Windows.Forms;

namespace calculator
{
    public partial class Main : Form
    {
        Keypad CalcKeypad = new Keypad();
        Data Calculator = new Data();
        public Main()
        {
            InitializeComponent();                        
        }        

        private void Main_Load(object sender, EventArgs e)
        {
            SetAppMenuColors();            
        }

        private void SetAppMenuColors()
        {
            AppMenu.Renderer = new ToolStripProfessionalRenderer(new AppMenuColorTable());
        }

        private void ZeroButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(0);
            UpdateDisplay();
        }

        private void OneButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(1);
            UpdateDisplay();
        }

        private void TwoButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(2);
            UpdateDisplay();
        }

        private void ThreeButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(3);
            UpdateDisplay();
        }

        private void FourButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(4);
            UpdateDisplay();
        }

        private void FiveButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(5);
            UpdateDisplay();
        }

        private void SixButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(6);
            UpdateDisplay();
        }

        private void SevenButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(7);
            UpdateDisplay();
        }

        private void EightButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(8);
            UpdateDisplay();
        }

        private void NineButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(9);
            UpdateDisplay();
        }

        private void CommaButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(10);
            UpdateDisplay();
        }

        private void DELButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(11);
            UpdateDisplay();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(12);
            UpdateDisplay();
        }

        private void SubButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(13);
            UpdateDisplay();
        }

        private void MultiplyButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(14);
            UpdateDisplay();
        }

        private void DivButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(15);
            UpdateDisplay();
        }                    

        private void CEButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(16);
            UpdateDisplay();
        }
        

        private void NegativeButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(17);
            UpdateDisplay();
        }

        private void UpdateDisplay()
        {
            DisplayTextbox.Text = Calculator.DataToDisplay;
        }

        private void ResultButton_Click(object sender, EventArgs e)
        {
            Calculator.PreformActionOnData(18);
            UpdateDisplay();
        }

       
    }
}
